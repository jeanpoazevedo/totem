<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Tiago Santos</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Graduação em Teologia – Centro Universitário Adventista de São Paulo - Unasp (2011).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Pastor Auxiliar – Associação Central Paranaense - ACP (2012 a 2013).</li>
        <li>Pastor Distrital – Associação Central Paranaense - ACP (2013 a 2015).</li>
        <li>Diretor de Departamento – Associação Central Paranaense - ACP (2016 a 2020).</li>
        </ul>
        <p><strong>Ministério Jovem</strong></p>
        <p>“O Senhor designou os jovens para serem Sua mão auxiliadora.”&nbsp;(Testemunhos Seletos, vol. 3, p. 104)</p>
        <p>Ser as mãos e os pés de Cristo em uma geração antagônica a Cristo e Seus princípios tem sido a marca dos nossos jovens. Com criatividade, ousadia e vontade de mudar o mundo, eles se mostraram ativos na tarefa de proclamar o Evangelho em nossa associação.</p>
        <p>Criar estratégias, ferramentas e projetos para potencializar a energia que naturalmente eles possuem foi a ênfase do nosso ministério nesse quadriênio, que marcou o momento histórico da atuação exclusiva com jovens e universitários.</p>
        <p>Louvamos a Deus pelo crescimento que vimos nos últimos quatro anos, mas acima de tudo pelas vitórias espirituais, dons desenvolvidos e vidas que foram salvas pelo esforço dos nossos jovens.</p>
        <p><strong>Projetos que envolve</strong><strong>m os jovens</strong><strong> na Missão</strong></p>
        <p>Oportunizar formas de viver o cristianismo em sua essência é uma das razões da <strong>Missão Calebe</strong> existir. Através de ações sociais, comunitárias e evangelísticas, nossos jovens abriram mão das férias para levar Cristo em todo território de nossa associação, sendo as mãos e os pés de Cristo com cada pessoa que foi abençoada através desse projeto.</p>
        <p>Nesse último quadriênio, 8.500 jovens se envolveram ativamente nos projetos e ações da Missão Calebe, atuando de forma relevante em nossas comunidades, sendo uma ferramenta poderosa de testemunho e aproximação de pessoas para o Reino de Cristo. Louvamos a Deus pelas muitas decisões tomadas, frutos do trabalho de nossos jovens calebes.</p>
        <p>O projeto <strong>Um Ano em Missão</strong> apresenta para os nossos jovens o desafio de&nbsp;doar um ano de suas vidas em prol da missão. Unindo seus esforços, talentos e habilidades, nossos missionários estão contribuindo para o avanço da Evangelho em nosso território. Visitar lares, realizar projetos sociais, abrir clubes de desbravadores e aventureiros e ministrar estudos bíblicos, são algumas das atividades desenvolvidas por eles.</p>
        <p>No último quadriênio, 55 jovens tiveram a oportunidade de vivenciar essa experiência única de serviço e transformação de vidas. Como resultado, tivemos o privilégio de participar do plantio de duas novas igrejas em nosso território, impactando as comunidades ao mesmo tempo em que a vida dos nossos missionários era impactada também.</p>
        <p>O ímpeto missionário dos nossos jovens não fica restrito ao território de nossa associação. Com uma visão de contribuir com o avanço do Evangelho em todo o mundo, o projeto <strong>Send</strong><strong> Me</strong> tem enviado jovens para ter uma experiência de missão em outras culturas.</p>
        <p>Nesses últimos quatro anos, várias comunidades indígenas e ribeirinhas da Amazônia foram impactadas. Além disso, nossos jovens foram para outros territórios do mundo, como Newark (EUA), Bishmizzine (Líbano) e Lima (Peru), espalhar amor e esperança. Foram 107 jovens que tiveram a oportunidade de viver em outras culturas o seu cristianismo prático, deixando marcas que perdurarão por toda a eternidade.</p>
        <p><strong>Programas</strong><strong> que</strong><strong>&nbsp;</strong><strong>desenvolvem </strong><strong>a liderança e o discipulado</strong></p>
        <p>Assim como em todos os ministérios da Igreja, a <strong>Escola Sabatina</strong> também é o coração do Ministério Jovem em nossa associação. Todos os nossos projetos estão vinculados à Escola Sabatina Jovem, fazendo dela a nossa grande agência de discipulado e pastoreio.</p>
        <p>Nos últimos quatro anos, tivemos a oportunidade de ver os seguintes percentuais de crescimento nos índices:&nbsp;Comunhão: (21%), Relacionamento (65%), Missão (43%) e Estudos Bíblicos (12,5%).</p>
        <p>Como parte da Escola Sabatina Jovem, tivemos o programa de customizar os espaços onde as reuniões acontecem, envolvendo os jovens no processo de dar uma identidade à classe. Atualmente, temos 52 salas customizadas em nosso território, perfazendo um 56% das salas jovens.</p>
        <p>A vida em comunidade não é apenas uma opção de programa para nossos jovens, mas um estilo de vida característico dessa geração. Para atender às&nbsp;necessidades de estarem próximos, desenvolver amizades e viver a experiência de discipulado e pastoreio em pequenos grupos, existe a <strong>Geração 148</strong>. Com encontros semanais nas casas, nossos jovens estão experimentando as bênçãos que a vida em comunidade pode trazer, tais como desenvolvimento dos dons, estudo da Bíblia e fortalecimento dos vínculos de amizade.</p>
        <p>Nos últimos quatro anos, essa ênfase foi fortalecida com treinamentos específicos para esse projeto, desenvolvimento de guias de estudos, abertura de novas bases e fortalecimento dos grupos já existentes. Atualmente temos 161 bases ativas em nossa associação.</p>
        <p>Treinar e motivar nossos jovens é uma preocupação constante do nosso ministério. Para termos os objetivos desse treinamento satisfeitos, o <strong>Save</strong><strong>&nbsp;</strong><strong>One</strong> traz uma proposta diferenciada. Com atividades lúdicas, demonstrações práticas, ideias já testadas, aliadas a muita motivação e troca de ideias, o Save&nbsp;One é a forma como nossos jovens foram treinados nesses últimos quatro anos.</p>
        <p>Vimos um crescimento expressivo nesses treinamentos, o que resultou em um ministério mais forte e efetivo nas igrejas locais. Nesse último quadriênio, tivemos 2.440 jovens treinados e capacitados para servir em suas igrejas – um crescimento de 532% se comparado com o quadriênio anterior.</p>
        <p>A cada ano, nossos jovens celebram nas igrejas o <strong>Dia Mundial J</strong><strong>A</strong>,&nbsp;com propósito de testemunhar sua fé. Mas esse dia não fica restrito às paredes da igreja, vamos às ruas, atendendo às comunidades com doação de sangue, atendimento a pessoas em situação de risco e vulnerabilidade social.</p>
        <p>Esse dia marca o início das nossas <strong>Celebrações Distritais</strong>, um culto jovem mensal com a cara dos jovens e com a essência de um culto de adoração composto por música, oração, testemunho e mensagem bíblica. Nesses últimos quatro anos, experimentamos várias iniciativas de cultos jovens, que serviram para fortalecer os jovens e ser uma ponte evangelística com os amigos.</p>
        <p><strong>Testemunhando no meio universitário</strong></p>
        <p>O momento da vida universitária é um grande desafio para nossos jovens, mas apresenta também inúmeras possibilidades. Em universidades públicas e privadas, temos um verdadeiro exército de jovens, que, se bem preparados e motivados, podem ser uma grande influência, tanto na comunidade acadêmica quanto no contato pessoal com seus colegas e professores.</p>
        <p>Para fortalecer o compromisso com Deus, bem como apresentar maneiras e argumentos para testemunharem nesse mundo das universidades, desenvolvemos no último quadriênio três grandes encontros para universitários em nossa associação. Além disso, iniciamos um projeto de consagração e envio a cada início de ano, chamado <strong>Culto de Envio</strong>. Ali, nossos jovens são enviados para testemunhar e ser uma luz a brilhar em cada universidade.</p>
        <p><strong>Ministério da </strong><strong>Música</strong></p>
        <p>“A música é um dos meios mais eficientes para impressionar o coração com verdades espirituais.” (Review and&nbsp;Harold, 6 de junho de 1912).</p>
        <p>Crendo nessa verdade revelada por Ellen White, o Ministério da Música se ocupou em treinar e capacitar nossos líderes de música, corais, bandas e conjuntos, bem como fornecer conhecimento técnico e prático para todos os músicos de nossa associação.</p>
        <p>Para contribuir com o ministério das igrejas locais, fizemos três grandes encontros de músicos, além de treinamentos com líderes locais. Pensando também nas igrejas que ainda não possuem instrumentistas, desenvolvemos o <strong>Projeto Doxologias</strong>, com a gravação e disponibilização de cinco músicas de doxologia, bem como suas partituras e vídeos, com o objetivo de tornar o culto ainda mais espiritual.</p>
        <p><strong>Ministério da </strong><strong>Comunicação</strong></p>
        <p>Tornar a comunicação cada vez mais efetiva, resultando em uma igreja mais informada e engajada em todas as iniciativas, é o propósito do departamento de Comunicação. Em um mundo no qual a comunicação se dá de forma cada vez mais rápida e diversificada, o departamento da Associação se ocupou em treinar e acompanhar nossos líderes em cada uma das nossas igrejas.</p>
        <p>Em três encontros de comunicadores e técnicos de som realizados no último quadriênio, nossos líderes locais foram treinados com tendências de comunicação, ferramentas disponíveis, valorização da função do comunicador, além de aulas técnicas e práticas para técnicos de som, com o objetivo de tornar a mensagem da salvação cada vez mais clara e eficiente.</p>
    </body>
</html>