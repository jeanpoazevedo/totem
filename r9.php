<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Rogério Vidal De Almeida </strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Graduação em Administração de Empresas – Universidade do Vale do Itajaí -&nbsp;Univali (2008).</li>
        <li>Graduação em Teologia – Centro Universitário Adventista de São Paulo -&nbsp;Unasp-EC (2014).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Pastor escolar – Colégio Adventista Boa Vista –<strong>&nbsp;</strong>Associação Central Paranaense - ACP<strong>&nbsp;</strong>(2015-2016).</li>
        <li>Diretor adjunto – Colégio Adventista Boqueirão<strong>&nbsp;</strong>–<strong>&nbsp;</strong>Associação Central Paranaense - ACP<strong>&nbsp;</strong>(2017).</li>
        <li>Pastor distrital<strong>&nbsp;</strong>–<strong>&nbsp;</strong>Associação Central Paranaense - ACP&nbsp;(2018-2019).</li>
        <li>Diretor de Departamento – Ministério de Desbravadores e Aventureiros (MDA)<strong>&nbsp;</strong>–<strong>&nbsp;</strong>Associação Central Paranaense - ACP<strong>&nbsp;</strong>(2020).</li>
        </ul>
        <p>Ser parte do povo que aguarda a breve volta de Jesus é uma bênção. Aceitar o chamado para servi-Lo como ministro é um privilégio. Fazer parte do Ministério de Desbravadores e Aventureiros (MDA) é uma honra.&nbsp;Estamos vivendo o tempo do fim, e para este tempo o MDA tem a missão e compromisso de preparar nossos desbravadores e aventureiros para o encontro com Cristo.</p>
        <p>Tive o privilégio de liderar o MDA na reta final deste quadriênio. Mesmo em meio às adversidades em decorrência da pandemia, louvamos a Deus porque, independente das limitações, avançamos. Hoje, podemos dizer: “Até aqui nos ajudou o Senhor” (I Samuel 7:12).</p>
        <p><strong>O crescimento gerado pela</strong><strong> n</strong><strong>ova </strong><strong>e</strong><strong>strutura</strong><strong> do</strong><strong> MDA </strong></p>
        <p>Em 2017, o território da ACP foi reorganizado, sendo criadas cinco áreas; estas divididas em 16 regiões e contando com uma equipe de aproximadamente 100 integrantes.&nbsp;Com a nova organização, tivemos aumentos na abertura de novos clubes, nas decisões por batismo e na frequência da equipe MDA nas reuniões e atividades dos clubes e solidificação dos clubes existentes.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Departamento</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Aventureiros</th>
                            <td>2.465</td>
                            <td>2.510</td>
                            <td>2.888</td>
                            <td>2.384</td>
                        </tr>
                        <tr>
                            <th scope="row">Desbravadores</th>
                            <td>4.262</td>
                            <td>4.849</td>
                            <td>4.443</td>
                            <td>4.226</td>
                        </tr>
                        <tr>
                            <th scope="row">Total</th>
                            <th>6.727</th>
                            <th>7.359</th>
                            <th>7.331</th>
                            <th>6.610</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>(Foto da distribuição das áreas e regiões)</p>
        <p>Essa estrutura foi fundamental para o crescimento do MDA, resultando em dados significativos.</p>
        <p>Clubes:</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Departamento</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Aventureiros</th>
                            <td>96</td>
                            <td>106</td>
                            <td>109</td>
                            <td>99</td>
                        </tr>
                        <tr>
                            <th scope="row">Desbravadores</th>
                            <td>147</td>
                            <td>150</td>
                            <td>149</td>
                            <td>140</td>
                        </tr>
                        <tr>
                            <th scope="row">Total</th>
                            <th>243</th>
                            <th>256</th>
                            <th>258</th>
                            <th>239</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <img src="r9/1.png" alt="image" class="img-responsive img-rounded"/>
            </div>
            <div class="col-sm-3"></div>
        </div>
        <p>Com o atendimento mais próximo dos clubes, pudemos realizar um trabalho de manutenção e fortalecimento, tendo como resultado clubes sólidos.</p>
        <p>Membros:</p>
        <p>Consequentemente, podemos contribuir todos os anos com o crescimento do Reino de Cristo, através das decisões para o batismo dos aventureiros e desbravadores.</p>
        <p>Batismos:</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Faixa Etária</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Aventureiros (6-9 anos)</th>
                            <td>137</td>
                            <td>113</td>
                            <td>177</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <th scope="row">Desbravadores (10-15 anos)</th>
                            <td>475</td>
                            <td>301</td>
                            <td>421</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <th scope="row"></th>
                            <th>612</th>
                            <th>414</th>
                            <th>598</th>
                            <th>51</th>
                        </tr>
                        <tr>
                            <th scope="row">Total ACP</th>
                            <td>2.146</td>
                            <td>1.375</td>
                            <td>1.770</td>
                            <td>221</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>Perceber-se que batismos com faixa etária correspondentes a aventureiros e desbravadores equivalem a uma média de 30% de todos os batismos da ACP.</p>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <img src="r9/2.png" alt="image" class="img-responsive img-rounded"/>
            </div>
            <div class="col-sm-3"></div>
        </div>
        <div class="row">
            <div class="col-sm-4 control-label">
                <img src="r9/3.png" alt="image" class="img-responsive img-rounded"/>
            </div>
            <div class="col-sm-4 control-label">
                <img src="r9/4.png" alt="image" class="img-responsive img-rounded"/>
            </div>
            <div class="col-sm-4 control-label">
                <img src="r9/5.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p><strong>Capacitações e grandes encontros do quadriênio</strong></p>
        <p>Neste quadriênio, tivemos um aumento significativo na formação de novos líderes. O MDA realizou cursos e treinamentos a nível de associação e incentivou os candidatos a participarem das classes de líderes. Podemos destacar a mudança no formato do curso, sendo que este passou a ser em sua grande maioria prático e menos teórico, tanto na área física, como mental e espiritual.&nbsp;</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Faixa Etária</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Aventureiros</th>
                            <td>18</td>
                            <td>15</td>
                            <td>27</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">Desbravadores</th>
                            <td>35</td>
                            <td>12</td>
                            <td>58</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">Master</th>
                            <td>7</td>
                            <td>1</td>
                            <td>5</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">M. Avançado</th>
                            <td>4</td>
                            <td>-</td>
                            <td>-</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th scope="row">Total</th>
                            <th>64</th>
                            <th>28</th>
                            <th>90</th>
                            <th></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <img src="r9/6.png" alt="image" class="img-responsive img-rounded"/>
            </div>
            <div class="col-sm-3"></div>
        </div>
        <p><strong>(Foto Mega Líder, Curso para Líder e Investidura)</strong></p>
        <p>Para que toda a equipe de apoio&nbsp;tenha a mesma visão, no início de cada ano é realizado o <strong>Concílio de Regionais</strong>, no qual orientamos, ensinamos e preparamos os coordenadores, regionais e distritais que atendem igrejas e clubes. Logicamente, um ponto alto dos nossos concílios é o preparo espiritual, que norteia toda as demais atividades.</p>
        <p><strong>(Foto Concílios)</strong></p>
        <p>Investimos nos aspectos espirituais dos clubes, pois&nbsp;existimos com o propósito de apresentar Cristo para nossos aventureiros e desbravadores. Pensando nisso, o MDA realizou o <strong>T</strong><strong>reinamento para Capelães</strong>, no qual eles foram preparados nas diversas áreas de atuação, tais como: dar um estudo bíblico, fazer um apelo, atender e entender as novas gerações, criatividade no estudo bíblico, elaboração de meditações criativas para o clube, atendimento familiar, apoio às atividades da igreja, etc. O resultado é visível quando olhamos para as decisões pelo batismo.</p>
        <p><strong>(Foto Encontro Capelães)</strong></p>
        <p>Além<strong>&nbsp;</strong>apresentar a salvação, o Clube é um agente discipulador, formador de líderes. Ao ingressar, o aventureiro tem uma longa jornada, na qual ele desenvolve habilidades que o tornarão um grande líder. O mesmo acontece com os desbravadores, mesmo que não tenham passado pelo Clube de Aventureiros. Ambos desenvolvem as classes, que os tornam ainda mais preparados para o dia a dia do clube. Mas seu preparo não para por aí.&nbsp;Ao ingressar na liderança do clube, eles são desafiados a seguirem para o próximo nível, que é a investidura como líder.&nbsp;O objetivo é sempre ter uma liderança bem capacitada e preparada, formando novos discípulos.</p>
        <p>O MDA contribui na formação deles e os prepara para trabalharem nas diversas áreas do clube.&nbsp;Anualmente, realizamos o <strong>Curso de Treinamento Básico para Diretoria (CTBD/CTAD)</strong>. É uma capacitação completa e realizada por área.</p>
        <p>Uma mudança significativa ocorreu a partir de 2017, quando o departamento percebeu um aumento significativo na participação do curso. Sendo assim, em 2018 passamos a atender cada área da ACP de forma mais personalizada, mediante à realidade do dia a dia dos clubes. Ao invés de termos um curso de treinamento para toda a associação, agora realizamos cinco treinamentos, contemplando cada área que compõe o território da ACP. Elevamos o padrão do curso, contemplando outras áreas internas do clube, que antes não atendíamos.</p>
        <p><strong>(Foto CTBD)</strong></p>
        <p>Os pastores de nosso campo também foram orientados no programa chamado <strong>Pastori</strong>, realizado pelo MDA em parceria com o Ministério Jovem.&nbsp;As ênfases&nbsp;foram as novas gerações e a proposta “Em cada igreja um clube”. O encontro aconteceu nos moldes de um acampamento, onde os pastores precisaram construir abrigos com lareira, fazer uma fogueira, construir uma cama no abrigo, fazer uma panela, etc. O objetivo foi mostrar para o pastor a necessidade de participar ativamente das atividades do clube.</p>
        <p><strong>(Foto </strong><strong>Pastori</strong><strong>)</strong></p>
        <p>Setembro é um mês significativo para desbravadores e aventureiros, quando os clubes celebram a conclusão da classe bíblica.&nbsp;Esse tem sido um diferencial pelo formato e conteúdo apresentado, com estudos diferenciados contemplando cada faixa etária de forma exclusiva. Como resultado, muitos frutos desse trabalho têm sido colhidos, com a decisão pelo batismo de aventureiros e desbravadores. Sendo assim, clubes e distritos preparam um dia para a&nbsp;<strong>Celebração</strong><strong> da</strong><strong> Primavera</strong>, reconhecendo o esforço, dedicação e decisão de cada integrante do clube.</p>
        <p><strong>(Foto Celebração e Classes Bíblicas)</strong></p>
        <p>Um campori é a celebração e a recompensa pelo trabalho desenvolvido nos clubes de desbravadores. Em 2017, foi realizado o <strong>4º</strong><strong>&nbsp;</strong><strong>Campori</strong><strong> da ACP</strong>, em Ponta Grossa, com 4.100 inscritos e 110 clubes.</p>
        <p>No âmbito missionário, a cidade de Ponta Grossa foi impactada com ações dos desbravadores, entrega de livros missionários, plantio de árvores e feira de saúde.</p>
        <p>Entre as diversas atividades, os desbravadores se envolveram em eventos com brincadeiras que deram ênfase no tema do campori, nas classes e especialidades. Os juvenis aprenderam sobre&nbsp;espaçomodelismo, arte de trançar, higiene oral, primeiros socorros básico, reanimação cardiopulmonar, sistema digestório e sistema respiratório na feira de especialidades. Também participaram dos concursos Bom de Bíblia, fanfarra, ordem unida e projeto Samuel.</p>
        <p>O tema “O Sonho Maior”, com base na vida de José, levou os desbravadores à decisão de se prepararem para alcançar o sonho maior, que é o Céu. No encerramento, uma grande coroa foi colocada no centro do auditório, onde cada diretor colocou uma lâmpada representando o seu compromisso junto com o seu clube de se preparar para o Céu.</p>
        <p>Em 2019, destacamos a participação dos Clubes de Desbravadores da ACP no <strong>5º</strong><strong>&nbsp;</strong><strong>Campori</strong><strong> Sul-Americano</strong> “A Melhor Aventura”, com 52 clubes e 2000 desbravadores.</p>
        <p><strong>(Foto </strong><strong>Camporis</strong><strong>) </strong></p>
        <p>Os aventureiros são motivo de louvarmos a Deus, pelo crescimento significativo dos nossos clubes, bem como seu envolvimento nas atividades locais e da Associação. Como resultado, durante o período tivemos a realização de três aventuris:</p>
        <ul>
        <li>2017 - “O Rei dos Sonhos” – Ponta Grossa-PR, com 65 clubes e 1900 inscritos;</li>
        <li>2018 - “Uma Aventura no Reino” – Campina Grande do Sul-PR, com 61 clubes e 1800 inscritos;</li>
        <li>2019 - “Aventussauro” – Castro-PR, com 79 clubes e 2100 inscritos.</li>
        </ul>
        <p><strong>Envolvimento dos desbravadores e aventureiros </strong><strong>em</strong><strong> programas da Igreja</strong></p>
        <p>Nosso objetivo é ser e fazer discípulos.&nbsp;Por isso, incentivamos os clubes a participarem de todos os projetos propostos pela ACP e igreja local, como Impacto Esperança, Semana Santa e Quebrando o Silêncio.</p>
        <p>Há também projetos do MDA que promovem o envolvimento com a igreja local. No <strong>Clube em Adoração</strong>, os<strong>&nbsp;</strong>clubes participam ativamente nos cultos de quarta-feira, realizando a programação uma vez por mês. A pregação nesse dia&nbsp;é realizada preferencialmente por um aventureiro ou desbravador, ou então um integrante da diretoria. As demais atividades do dia ficam sob responsabilidade do clube.</p>
        <p><strong>(Fotos Projetos)</strong></p>
        <p><strong>Agradecimentos</strong></p>
        <p>Olhando para trás, externamos nossa gratidão pelas inúmeras bênçãos e conquistas do departamento. De maneira especial, destacamos o incansável trabalho e dedicação do pastor Fabiano Souza. Registramos nossa gratidão a ele por tudo o que fez em prol dos nossos desbravadores e aventureiros. Obrigado!</p>
        <p>Nosso reconhecimento e gratidão à grande equipe que fez e faz o MDA acontecer, equipe esta constituída de pastores distritais e de escolas, secretárias, coordenadores, regionais, distritais e diretores de clubes. Nossa gratidão à administração da Associação Central Paranaense, que sempre esteve pronta a apoiar o MDA.</p>
    </body>
</html>