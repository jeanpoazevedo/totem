<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Julio</strong><strong> Cesar Padilha</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Graduação em Teologia – Centro Universitário Adventista de São Paulo - Unasp (1992).</li>
        <li>Mestrado em Teologia – Centro Universitário Adventista de São Paulo - Unasp (2004).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Pastor escolar – Associação Mato-grossense - AMT (1993-1994).</li>
        <li>Pastor distrital – Associação Mato-grossense - AMT (1995-2001).</li>
        <li>Pastor Distrital – Associação Catarinense - AC (2002-2005).</li>
        <li>Diretor Associado de Departamento – Associação Catarinense - AC (2006-2008).</li>
        <li>Diretor de Departamento – Associação Catarinense - AC (2009-2012).</li>
        <li>Diretor de Departamento – Associação Central Paranaense - ACP (2012-2020).</li>
        </ul>
        <p><strong>“</strong>Ide, portanto, fazei discípulos de todas as nações, batizando-os em nome do Pai, e do Filho, e do Espírito Santo; ensinando-os a guardar todas as coisas que vos tenho ordenado. E eis que estou convosco todos os dias até à consumação do século.” (Mateus 28:19-20)</p>
        <p><strong>Escola Sabatina </strong></p>
        <p>No último quadriênio, a prioridade da Associação Central Paranaense foi marcada pela busca contínua de seguir a ordem do Mestre: formar novos discípulos,&nbsp;conforme registrado no evangelho de Mateus 28:19-21. Para atingir esse objetivo, o campo utilizou a estratégia da rede de discipulado, que se desenvolve em quatro fases, sendo a primeira com o grupo de administradores e departamentais da associação, o chamado Pequeno Grupo de Departamentais (PGD).</p>
        <p>A temática trabalhada nesse grupo serve de agenda para a segunda reunião que acontece uma vez a cada dois meses com os pastores distritais (PGP). Uma vez que o discipulado está no coração do pastor, ele organiza um Pequeno Grupo de Líderes (PGL), que é formado por professores da Escola Sabatina e pelos anciãos, uma vez por mês. Nessas reuniões, são debatidos temas sobre como os membros da igreja, que são alunos das <strong>unidades de ação</strong>, podem se tornar discípulos atuantes na comunidade, atendendo à necessidade das pessoas da comunidade e testemunhando do amor de Jesus.</p>
        <p>Fotos PGP</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r5/1.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>Fotos PGL</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r5/2.png" alt="image" class="img-responsive img-rounded"/>
            </div>
            <div class="col-sm-6">
                <img src="r5/3.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>No processo de discipulado, o objetivo final é ver a igreja em ação, transformando os membros inativos em ativos, indo até a comunidade, levando o alimento espiritual, emocional e, se houver necessidade, o alimento físico, tendo a Escola Sabatina como a plataforma de mobilização.</p>
        <p>O sistema da Escola Sabatina, comunhão, relacionamento e ação, bem como o incentivo permanente dos professores, permitiu mensurar o processo de desenvolvimento dos discípulos, através do testemunho semanal da maioria dos membros das unidades. É notório, pela graça de Deus, um crescimento contínuo e saudável a cada trimestre.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>CRM ACP</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>Total Membros</th>
                            <th>Membros presentes na ES</th>
                            <th>Comunhão</th>
                            <th>Relacionamento</th>
                            <th>Missão</th>
                            <th>Membros dando estudos bíblicos</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>25.374</td>
                            <td>9.563 (38%)</td>
                            <td>6.147 (24%)</td>
                            <td>5.630 (22%)</td>
                            <td>5.378 (21%)</td>
                            <td>913 (4%)</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>23.379</td>
                            <td>10.741 (46%)</td>
                            <td>7.217 (31%)</td>
                            <td>6.799 (29%)</td>
                            <td>6.388 (27%)</td>
                            <td>1.372 (6%)</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>23.264</td>
                            <td>11.631 (50%)</td>
                            <td>8.232 (35%)</td>
                            <td>8.005 (34%)</td>
                            <td>7.546 (32%)</td>
                            <td>1.850 (8%)</td>
                        </tr>
                        <tr>
                            <th scope="row">2020</th>
                            <td>23.201</td>
                            <td>11.586 (50%)</td>
                            <td>8.011 (34%)</td>
                            <td>7.820 (34%)</td>
                            <td>7.139 (31%)</td>
                            <td>1.311 (6%)</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>“Se você não pode medir algo, você não pode melhorá-lo” (Wiliam Thomson)</p>
        <p><strong>Programa de c</strong><strong>apacitação de l</strong><strong>í</strong><strong>deres da Escola Saba</strong><strong>tina, Ministério Pessoal e ASA</strong></p>
        <p>Inspirado na dinâmica de discipulado de Jesus, que contemplava o chamado, a capacitação e o envio (Marcos 3:14),&nbsp;e seguindo as orientações do Espírito de Profecia de capacitar os líderes da igreja, Ellen White também orienta que a capacitação de líderes é prioridade do ministério, conforme texto: “O maior auxílio que se pode prestar a nosso povo, é ensiná-lo a trabalhar para Deus e a nEle confiar, e não nos pastores. Aprendam a trabalhar como Cristo trabalhou. Unam-se ao Seu exército de obreiros, e façam por Ele trabalho fiel.” (Testemunhos Seletos, Vol. 3, p. 82).</p>
        <p>Os departamentos do Ministério Pessoal, Escola Sabatina e ASA priorizaram a formação dos líderes no quadriênio através de um programa contínuo de formação de líderes, que foram as trimestrais. Mesmo no período da pandemia, os treinamentos continuaram de forma remota, proporcionando um alcance recorde de assistência. Mais de mil líderes foram capacitados em 2020.&nbsp;</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r5/4.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r5/6.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <img src="r5/5.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p><strong>Vintage </strong></p>
        <p>O departamento do Ministério Pessoal da ACP, em parceria com o departamento de Missão Global da USB, realizou no quadriênio o<strong> Vintage</strong>, que é um<strong>&nbsp;</strong>movimento de revitalização de igrejas missionais, o qual visa desenvolver as<strong>&nbsp;</strong>práticas missionais no escopo pastoral que reforçam um compromisso com o evangelho, a cidade e a missão. Também visa ensinar a como desenvolver sistemas de revitalização, replantio e plantação de igrejas saudáveis. O total de 512 líderes da região metropolitana de Curitiba e do interior foram capacitados.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r5/7.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <img src="r5/8.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <img src="r5/9.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p><strong>Ministério Pessoal</strong></p>
        <p>O papel do departamento do Ministério Pessoal no contexto do discipulado é de extrema relevância, pois sua missão é mobilizar os que já são discípulos a formarem novos. Para isso, a Associação Central Paranaense priorizou a formação missionária,&nbsp;estabelecendo as escolas missionárias nos distritos, que é uma recomendação do Espírito de Profecia para o desenvolvimento das práticas e do ímpeto evangelístico que deve haver em cada discípulo.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r5/10.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>&nbsp;“Toda igreja deve ser uma <strong>escola missionária</strong> para obreiros cristãos. Seus membros devem ser instruídos em dar estudos bíblicos, na melhor maneira de auxiliar os pobres e cuidar dos&nbsp;doentes, de trabalho pelos não convertidos.&nbsp;Não somente deve haver ensino, mas trabalho real, sob a direção de instrutores experientes.”&nbsp;(A Ciência do Bom Viver, p. 149)</p>
        <p>Um dos destaques do campo no quadriênio foi o crescente envolvimento dos membros no evangelismo de semeadura e colheita da <strong>Semana Santa</strong>, responsável por muitas decisões durante o ano.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b><strong>Entradas</strong>: Batismos, Rebatismo e Profissão de Fé</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>Pontos de Pregação</th>
                            <th>Interessados</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>1518</td>
                            <td>13.868</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>1530</td>
                            <td>9.533</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>1897</td>
                            <td>10.883</td>
                        </tr>
                        <tr>
                            <th scope="row">2020</th>
                            <td>8.620</td>
                            <td>15.388</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p><strong>A</strong><strong>ção </strong><strong>S</strong><strong>olidária </strong><strong>A</strong><strong>dventista (ASA)</strong></p>
        <p>A relevância da ASA na igreja, em especial na comunidade, sempre foi visível. Os núcleos presentes em cada igreja realizaram um trabalho virtuoso de atendimento às famílias mais vulneráveis da sociedade. Ações como entrega sistemática de cestas básicas, entrega de roupas e cursos de capacitação profissional, fazem parte do cotidiano dessas bases da ASA de cada igreja espalhadas pelo campo.</p>
        <p>No entanto, é notória a atuação das equipes na igreja e na comunidade no período da pandemia. Muitas famílias, em especial as da igreja, foram atendidas com alimentos e roupas. Mais de 120 toneladas de cestas básicas foram entregues até o mês de julho de 2020.</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r5/11.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r5/13.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <img src="r5/12.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
    </body>
</html>