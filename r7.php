<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Julio</strong><strong> Diniz</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Graduação em Teologia – Centro Universitário Adventista de São Paulo - Unasp (2009).</li>
        <li>Pós-Graduação em Aconselhamento Familiar – Sociedade Paranaense de Ensino e Informática - SPEI (2010).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Pastor escolar – Colégio Adventista Boqueirão (2010).</li>
        <li>Pastor distrital - Associação Central Paranaense - ACP (2011-2017).</li>
        <li>Diretor de Departamento - Associação Central Paranaense - ACP (2017-2020).</li>
        </ul>
        <p><strong>Evangelismo</strong></p>
        <p>“Quão formosos são sobre os montes os pés do que anuncia boas-novas, que faz ouvir a paz, que anuncia coisas boas, que faz ouvir a salvação, que diz a Sião: O seu Deus Reina!” (Isaías 52:7)</p>
        <p><strong>Campanha</strong><strong>s</strong><strong> evangelísticas</strong><strong> e plantio de novas igrejas</strong></p>
        <p>Com total envolvimento dos membros locais, parceria com o projeto “Um Ano em Missão”,&nbsp;apoio incondicional da ACP e Igreja do Boqueirão, o Evangelho chegou aonde praticamente não havia presença adventista.&nbsp;Nos três últimos anos, foram realizadas campanhas evangelísticas de grande escala e de impacto para que as comunidades recebessem um templo adventista, muitos conhecessem a verdade da Bíblia e fossem batizados.</p>
        <p>Em 2017, o evangelismo levou ao batismo 48 pessoas na cidade de Ipiranga, distrito de Ponta Grossa.</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r7/1.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r7/2.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Em Ponta Grossa, no bairro Costa Rica, distrito Jardim Esplanada, 67 pessoas foram batizadas em 2018.</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r7/3.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r7/4.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Foram 83 batismos em 2019 na cidade de Castro, bairro Jardim Alvorada, distrito Vila Rio Branco.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r7/5.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>O apóstolo Paulo um dia aconselhou Timóteo com as seguintes palavras: “...prega a palavra, insta, quer seja oportuno, quer não, corrige, repreende, exorta com longanimidade e doutrina.”&nbsp;(II Timóteo 4:2). Sendo assim, nossos pastores, obreiros bíblicos, líderes locais e membros como um todo, a cada ano, têm se esforçado e realizado a pregação da verdade através de campanhas evangelísticas, sejam elas com duração de uma semana ou de finais de semana.</p>
        <p>Nosso papel como associação é incentivar, capacitar e fornecer todo recurso possível para que as campanhas aconteçam.&nbsp;Nos últimos quatro anos, foram realizadas em todo nosso território mais de 890 campanhas evangelísticas:</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>Nº de Campanhas Evangelísticas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>218</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>246</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>284</td>
                        </tr>
                        <tr>
                            <th scope="row">2020</th>
                            <td>143</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <img src="r7/6.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r7/7.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Além dos evangelismos públicos, temos a preocupação em atender uma demanda cada dia mais especifica.&nbsp;A&nbsp;<strong>Série Especial de Estudos Bíblicos Direcionados</strong> busca facilitar o alcance do Evangelho a diferentes grupos de pessoas que se identifiquem com a linguagem do material desenvolvido. A finalidade é apresentar o conteúdo bíblico com uma roupagem de afinidade com diferentes nichos de interesses sociais.</p>
        <p>A Rede Novo Tempo (rádio e televisão) é um agente poderoso que a Igreja tem usado na pregação do Evangelho. Todos os dias, milhares de ouvintes e telespectadores que residem em nossa geografia são impactados com as boas novas de Salvação através dos programas evangelísticos apresentados. Muitas dessas pessoas recebem em suas casas materiais com conteúdo bíblico, como DVDs e estudos.</p>
        <p>Como associação, procuramos dar continuidade a esse trabalho de uma forma mais pessoal, contactando e promovendo a visitação através de membros devidamente treinados e preparados.&nbsp;Uma vez por ano, realizamos um evento musical e evangelístico intitulado <strong>Abrace Novo Tempo</strong>, com a presença de um cantor e um pregador da TV Novo Tempo. Nesse programa, que acontece nas três principais regiões do nosso campo, centenas de telespectadores têm a chance de tomar a decisão por Jesus e depois serem batizadas.</p>
        <p>Em 2018, tivemos o total de 2.084 pessoas presentes em nossos programas, sendo 1.680 interessados, dos quais 631 atenderam ao apelo. No ano seguinte, o público total foi de 2.730 pessoas, 1.700 interessados e 646 que atenderam ao apelo.</p>
        <p><strong>2018 </strong></p>
        <p>Guarapuava:</p>
        <ul>
        <li>Total de pessoas presentes: 879</li>
        <li>Total de interessados: 446</li>
        <li>Aceitaram apelo: 129 pessoas</li>
        </ul>
        <p>Ponta Grossa:</p>
        <ul>
        <li>Total de pessoas presentes: 847</li>
        <li>Total de interessados: 566</li>
        <li>Aceitaram apelo: 223 pessoas</li>
        </ul>
        <p>Curitiba:</p>
        <ul>
        <li>Total de pessoas presentes: 1078</li>
        <li>Total de interessados: 668</li>
        <li>Aceitaram apelo: 279 pessoas</li>
        </ul>
        <p><strong>2019 </strong></p>
        <p>Guarapuava:</p>
        <ul>
        <li>Total de pessoas presentes: 650</li>
        <li>Total de interessados: 400</li>
        <li>Aceitaram apelo: 120 pessoas</li>
        </ul>
        <p>Ponta Grossa:</p>
        <ul>
        <li>Total de pessoas presentes: 1400</li>
        <li>Total de interessados: 900</li>
        <li>Aceitaram apelo: 346 pessoas</li>
        </ul>
        <p>Curitiba:</p>
        <ul>
        <li>Total de pessoas presentes: 680</li>
        <li>Total de interessados: 400</li>
        <li>Aceitaram apelo: 180 pessoas</li>
        </ul>
        <div class="row">
            <div class="col-sm-6">
                <img src="r7/8.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r7/9.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p><strong>Capacitações para membros e pastores</strong></p>
        <p>Entendemos que a missão faz parte do DNA de todo aquele que nasce em Cristo.&nbsp;Sendo assim, procuramos incentivar e capacitar o maior número de membros possível para assumir o dom e a responsabilidade de testemunhar do amor de Deus, ministrando estudos bíblicos&nbsp;com espiritualidade, intencionalidade e a alegria de quem já foi salvo.</p>
        <p>No último quadriênio, mais de nove mil evangelistas voluntários foram capacitados em treinamentos com grandes públicos e em encontros distritais.</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r7/10.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r7/11.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Cada pastor chamado por Deus para o ministério traz consigo uma paixão e um desejo incomum de levar aos pés de Cristo pessoas pelas quais o Céu entregou O que havia de mais valioso.&nbsp;Porém, a rotina de trabalho que envolve o ministério é cheia de compromissos e requer do pastor envolvimento em várias frentes de atuação. Com a intenção de&nbsp; manter acesa e focada a chama do evangelismo, como associação, proporcionamos um período denominado <strong>Escola de Evangelismo Pastoral</strong>, em que o ministro recebe aulas teóricas e práticas, por meio de seminários, visitação e participando de uma semana evangelística na qual também se oportuniza aos seus interessados um momento de entrega e decisão pelo batismo.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r7/12.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <img src="r7/13.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p><strong>Mordomia</strong></p>
        <p>“Não se propõe o Senhor vir a este mundo e derramar ouro e prata para o avanço de sua obra. Supre os homens com recursos, para que pelas dádivas e ofertas&nbsp;conservem Sua obra em avanço.” (Conselhos sobre Mordomia,&nbsp;p. 36)</p>
        <p><strong>Encontros com os líderes</strong></p>
        <p>A fim de valorizar o trabalho incansável e altamente responsável realizado pelos tesoureiros de nossas congregações e ao mesmo tempo fortalecer o conhecimento teórico de nossos líderes de Mordomia, a Associação promoveu o <strong>1º Encontro de Mordomia da ACP</strong>. Neste encontro, contamos com a presença de liderança de Mordomia da União Sul Brasileira e da Divisão Sul-Americana.</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r7/14.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r7/15.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Mais do que nunca, entendemos que a mola propulsora dos grandes movimentos da Igreja para qualquer departamento ou projeto são os líderes locais juntamente com seus pastores. Por isso, no departamento de Mordomia da ACP, incentivamos <strong>Encontros Distritais</strong> com a liderança, com o objetivo de apresentar de forma simples e transparente a realidade dos índices de fidelidade de cada igreja.&nbsp;Assim, chegamos juntos a soluções práticas para que o nível de adoração se eleve e traga, como resultado, uma igreja mais benevolente, que reconhece a Deus como o Supremo Criador e Mantenedor de todas as coisas.</p>
        <p><strong>Mordomia em números</strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b><strong>Entradas</strong>: Batismos, Rebatismo e Profissão de Fé</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Percentual de membros dizimistas regulares. (1)</th>
                            <td>22,69%</td>
                            <td>23,65%</td>
                            <td>24,13%</td>
                            <td>23,75%</td>
                        </tr>
                        <tr>
                            <th scope="row">Percentual de membros pactuantes regulares. (1)</th>
                            <td>12,36%</td>
                            <td>12,88%</td>
                            <td>14,02%</td>
                            <td>12,91%</td>
                        </tr>
                        <tr>
                            <th scope="row">Valores de entradas de dízimos e percentual de crescimento. (2)</th>
                            <td>R$ 31.350.996,36 +2,87%</td>
                            <td>R$ 31.878.390,56 +1,68 %</td>
                            <td>R$ 33.365.388,18 +4,66%</td>
                            <td>R$ 25.342.834,90 (2) +0,78% (2)</td>
                        </tr>
                        <tr>
                            <th scope="row">Valores de entradas de ofertas e percentual de crescimento.</th>
                            <td>R$ 12.388.389,83 +1,20%</td>
                            <td>R$ 12.950.330,60 +4,54%</td>
                            <td>R$ 12.916.796,87 +0,26%</td>
                            <td>R$ 4.467.558,18 -10,41%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>(1)-&nbsp; Consideramos dizimistas e pactuantes regulares todos aqueles que, no período de 12 meses,&nbsp;apresentam de 8 a 12 doações.</p>
        <p>Obviamente, consideramos também as variadas formas e sazonalidades de fontes de renda dos doadores.</p>
        <p>(2)- Todos os valores apresentados são comparados com os valores do mesmo período do ano anterior. No caso de 2020, os valores apresentados representam o cumulativo até o mês de setembro.</p>
        <p><strong>Agradecimentos</strong></p>
        <p>Nossa gratidão aos pastores Washington Moraes, Gustavo Moroz e Amarildo dos Santos, que atuaram de forma primorosa&nbsp;nos treinamentos, capacitações dos membros envolvidos e na organização dos eventos do Abrace Novo Tempo.</p>
        <p>Agradecemos&nbsp;também o trabalho responsável e espiritual realizado pelo pastor André Flores até o ano de 2018 no departamento de Mordomia Cristã.</p>
    </body>
</html>