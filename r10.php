<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Rael Pereira Braz</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Licenciatura em Pedagogia – Centro Universitário Adventista de São Paulo - Unasp-HT (2004).</li>
        <li>Pós-Graduanda em Coordenação e Orientação Educacional – IAP (em curso).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Professora regente – Colégio Adventista Presidente Prudente-SP (1997).</li>
        <li>Professora regente – Colégio Adventista Boa Vista – Curitiba-PR (1998 a 1999).</li>
        <li>Professora regente – Colégio Adventista de Hortolândia-SP (2000 a 2002).</li>
        <li>Professora regente – Colégio Adventista de Limeira-SP (2004 a 2005).</li>
        <li>Coordenadora pedagógica – Colégio Adventista de Jundiaí-SP (2007).</li>
        <li>Diretora de Departamento – Associação Central Paranaense - ACP (2018 a 2020).</li>
        </ul>
        <p><strong>Ministério da Mulher</strong></p>
        <p>O Ministério da Mulher é um departamento muito especial, organizado há apenas 25 anos como status pleno de departamento, mas envolvente e dedicado desde o início da história da nossa igreja. Existe para manter, encorajar e desafiar as mulheres adventistas em sua peregrinação, como discípulas de Jesus Cristo e como membros da Sua igreja mundial. Nossa responsabilidade é com todas as mulheres: casadas, solteiras, mães, viúvas, divorciadas, jovens, estudantes, profissionais, etc.</p>
        <p>“O Senhor tem um trabalho para mulheres e homens. Elas podem ocupar seu lugar neste tempo de crise, e Ele trabalhará através delas. ... Elas podem fazer nas famílias um trabalho que os homens não podem fazer, um trabalho que alcança a vida interior. Elas podem se aproximar do coração daqueles a quem os homens não podem alcançar. Seu trabalho é necessário.” (Ministério do Bem&nbsp;Estar, p. 145).</p>
        <p><strong>P</strong><strong>rojetos para envolver as mulheres da igreja e ajudar as amigas da comunidade</strong></p>
        <p>Nossa principal ação é o <strong>Projeto MEL – Mulheres Evangelizadoras Levando Luz</strong>.<strong>&nbsp;</strong>O objetivo é ajudar as mulheres a serem semelhantes a Jesus ao manter uma vida de comunhão com Deus, de serviço e amor ao próximo, de desenvolvimento dos seus dons e talentos para a pregação do evangelho, tornando-se uma luz em sua casa, igreja e comunidade.</p>
        <p>Toda mulher pode ser investida, desde que participe do treinamento anual. Conforme os itens cumpridos, cada participante recebe a condecoração.</p>
        <p><strong>Mulheres investidas</strong><strong> no MEL</strong><strong>:</strong></p>
        <p>Nível 1 – Lenço</p>
        <ul>
        <li>2017: 1363</li>
        <li>2018:&nbsp;1494</li>
        <li>2019: 1462</li>
        <li>2020: 1290</li>
        </ul>
        <p>Nível 2 – Arganéu</p>
        <ul>
        <li>2017: 778</li>
        <li>2018: 976</li>
        <li>2019: 1451</li>
        <li>2020: 1009</li>
        </ul>
        <p>Nível 3 – Mulher Discipuladora</p>
        <ul>
        <li>2017: 335</li>
        <li>2018: 352</li>
        <li>2019: 458</li>
        <li>2020: 508</li>
        </ul>
        <p>O senso de pertencer faz parte do ser humano, e o projeto MEL promove, uma vez por ano, a <strong>Semana do lenço amarelo e o Sábado Missionário da Mulher Adventista. </strong>As participantes são desafiadas a usarem o lenço na sua rotina, terminado no primeiro sábado de junho, o sábado missionário. Nesse dia, elas celebram, testemunham, promovem ações solidárias, participam do treinamento do Projeto MEL, etc.</p>
        <p>Neste ano, a criatividade e a dedicação não deixaram a pandemia intimidar as mulheres missionárias, com programas lindos e especiais feitos para elas e organizados por elas. Tivemos 1.872 participações<strong>&nbsp;</strong>no <strong>Treinamento do Projeto MEL</strong> on-line. (Dados obtidos pelas coordenadoras distritais).</p>
        <p>Números de igrejas que realizaram o Sábado Missionário no Quadriênio:</p>
        <ul>
        <li>2017: 107</li>
        <li>2018: 110</li>
        <li>2019: 143</li>
        <li>2020: 93</li>
        </ul>
        <p>Para evangelizar é preciso orar. Por isso os&nbsp;<strong>10 Dias de Oração </strong>acontecem no início do ano e o Ministério da Mulher está unido com os outros departamentos da Igreja para fazer acontecer da melhor maneira possível. Fazem parte ações internas, para os membros da igreja, e ações externas, para a comunidade e vizinhança. Em 2020, o objetivo era trabalhar com os ex-adventistas. Colhemos testemunhos lindos e emocionantes.&nbsp;</p>
        <ul>
        <li>2017: 206</li>
        <li>2018: 201</li>
        <li>2019: 197</li>
        <li>2020: 199</li>
        </ul>
        <p>Uma vez por mês,&nbsp;realizamos o culto <strong>Quartas de Poder</strong>, com o objetivo de conseguir mais pessoas para participar, testemunhar e agradecer, proporcionando às mulheres a oportunidade de se envolverem na pregação.</p>
        <p>A cada ano temos um tema geral, os últimos três foram: Discípulos (2018),&nbsp;Restaurando o Altar&nbsp;(2019) e O Santuário do Lar (2020).&nbsp;A maioria das congregações participa. Na comunhão ainda temos o Grupo de Oração Intercessora, com temas para meditação semanal e vídeos apresentados para continuar motivando o grupo e a ter dependência de Deus.</p>
        <ul>
        <li>2017: 175</li>
        <li>2018: 156</li>
        <li>2019: 197*</li>
        <li>2020: 173</li>
        </ul>
        <p>*&nbsp; dados fornecidos pelas coordenadoras distritais e Relatório Integrado</p>
        <p>Nosso principal projeto social é o<strong> Quebrando o Silêncio</strong>, que tem o objetivo de conscientizar as pessoas e tirá-las do sofrimento, do abuso e violência, denunciando e realizando um trabalho que envolva todos os membros da igreja. Abordamos temas como: suicídio, violência infantil, violência doméstica, etc.<strong>&nbsp;</strong>Distribuímos materiais riquíssimos em conteúdo para crianças, jovens e adultos; disponibilizamos sermões, revistas&nbsp;para várias idades, vídeos e seminários para trabalhar nas igrejas, escolas e demais localidades.</p>
        <ul>
        <li>2017: 135</li>
        <li>2018: 154</li>
        <li>2019: 178</li>
        <li>2020: 41*</li>
        </ul>
        <p>* Dados do primeiro semestre do ano</p>
        <p><strong>Ministério da Recepção</strong></p>
        <p>O Ministério da Recepção é o cartão de visitas da igreja. Existe com objetivo maior de apenas receber bem as pessoas, e evangelizar é sua finalidade. A maneira como são tratadas vai definir se voltarão ou não à igreja.&nbsp;Por isso, cativar as pessoas para Cristo é a missão de vida de cada cristão.</p>
        <p>“A Igreja é o instrumento apontado por DEUS para a salvação dos homens. Foi organizada para servir, e sua missão é levar o evangelho ao mundo... Aos membros da igreja, a quem ELE chamou das trevas para Sua maravilhosa luz, compete manifestar Sua glória” (Atos dos Apóstolos, p. 9).</p>
        <p><strong>Ações da Recepção</strong></p>
        <p>Para sermos uma igreja acolhedora, que cuida de gente, amando, acolhendo, atendendo e acompanhando, trabalhamos com ações ritmadas. A cada semana, a recepção deve ser atuante em todos os programas da igreja, preencher o cartão de boas-vindas, repassar os pedidos de estudo e oração para o Mipes e o Grupo de Intercessão, e entrar em contato com as visitas no dia seguinte. Mensalmente, o ministério treina os membros com vídeos de conscientização e inspiração. A ação trimestral é promover uma atividade da igreja com os visitantes, como almoço, junta panelas, reuniões entre amigos, etc.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Ação Semanal</th>
                            <th>Ação Mensal</th>
                            <th>Ação Trimestral</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Ano</th>
                            <td>Sim</td>
                            <td>Sim</td>
                            <td>Sim</td>
                        </tr>
                        <tr>
                            <th scope="row">2017</th>
                            <td>153</td>
                            <td>68</td>
                            <td>122</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>152</td>
                            <td>72</td>
                            <td>776</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>153</td>
                            <td>81</td>
                            <td>124</td>
                        </tr>
                        <tr>
                            <th scope="row">2020</th>
                            <td>157</td>
                            <td>87</td>
                            <td>125</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p><strong>Os encontros do Ministério da Mulher e da Recepção</strong></p>
        <p>Todos os distritos têm uma coordenadora que é responsável pelo Ministério da Mulher e o Ministério da Recepção.&nbsp;A cada três meses, nos reunimos com elas para motivar, acompanhar o trabalho feito, analisar o relatório integrado e ver onde melhorar, trocar ideias, confraternizar e, assim, o trabalho continuar com foco e determinação.</p>
        <p>Em 2020, continuamos com reuniões pelo aplicativo Zoom e não desanimamos. Temos a bênção de relatar, através das coordenadoras distritais, o trabalho das mulheres neste ano. São 448 dando estudos bíblicos, 1073 que se engajaram na Semana Santa e 428 que estão preparando alguém para o batismo.</p>
        <p>As mulheres esperam ansiosas e animadas pelos retiros, congressos e simpósios. O Ministério da Mulher organizou nesse quadriênio alguns eventos só para elas. Em 2017: “Escolhidas” (Catre Santa Catarina). Em 2018: Simpósio ACP (Auditório Nepomuceno de Abreu): “O Papel da Mulher no Século XXI”; “Transformando em Pérolas” (Hotel Monthez, Brusque-SC). Em 2019: Congresso “Eu Mulher, nos Sonhos de Deus” (Ponta Grossa); Congresso ACP: “Floresça onde Deus te plantou” (Auditório Nepomuceno de Abreu). E por causa da pandemia, o próximo retiroserá&nbsp;será realizado em abril de 2021: “Em Busca da Felicidade” (Hotel Canasvieiras Florianópolis-SC).&nbsp;</p>
    </body>
</html>