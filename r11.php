<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Rita Maria Barbosa Assis</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Graduação em Pedagogia – Centro Universitário Adventista de São Paulo - Unasp (1984).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Telefonista no Unasp-SP (1981 a 1984).</li>
        <li>Bancária no Banco Bradesco e HSBC em Poços de Caldas-MG (1985 a 1989).</li>
        <li>Professora regente – Colégio de Cascavel-PR (1991 a 1992).</li>
        <li>Professora regente – Colégio de Terra Roxa-PR (1994 a 1995).</li>
        <li>Telefonista e Coordenadora Geral dos Aventureiros – Associação Sul Riograndense -ASR (1999 a 2002).</li>
        <li>Recepcionista – Colégio Adventista de Maringá-PR (2003 a 2005).</li>
        <li>Coordenadora Geral dos Aventureiros – Associação Norte Paranaense - ANP (2003 a 2005).</li>
        <li>Professora de Artes no Instituto Adventista Paranaense - IAP (2006 a 2007).</li>
        <li>Professora regente – Colégio Adventista de Caxias do Sul-RS (2008).</li>
        <li>Professora de Artes – Prefeitura de Caxias do Sul (2008).</li>
        <li>Diretora de Departamento – Ministério da Mulher – Associação Central Sul-Riograndense -ACRS (2009 a 2011).</li>
        <li>Secretária da Presidência - Associação Rio de Janeiro (2012 a 2015)</li>
        <li>Secretária de cadastro – Associação Central Paranaense - ACP (2015).</li>
        <li>Diretora de Departamento – Ministério da Criança e Adolescente – Associação Central Paranaense - ACP (2016 a 2020).</li>
        </ul>
        <p>Hoje, manter o interesse e atenção das novas gerações neste mundo conectado em que vivemos é um grande desafio. Os&nbsp;ministérios da Criança e do Adolescente da ACP, através de ações e iniciativas realizadas ao longo deste quadriênio, tem conquistado bons resultados e cumprido com esta missão.</p>
        <p>Com o pensamento de que nossos pequenos são o presente de nossa Igreja, temos investido tempo, desenvolvido dons e talentos, capacitando líderes para alcançá-los. Com isso, crianças e adolescentes foram envolvidos a vivenciarem o amor de Deus, fortalecendo princípios e valores, incentivados a serem sal e luz nas redes sociais onde estão conectados, nas escolas onde frequentam, com os amigos que convivem e em seus lares.</p>
        <p><strong>Ministério da Criança</strong></p>
        <p>Procuramos envolver ao máximo nossas crianças nas classes da Escola Sabatina:</p>
        <p>A cada trimestre, nossos coordenadores, professores e distritais, são capacitados e orientados em como trabalhar e dar suporte aos nossos meninos e meninas dentro e fora da Escola Sabatina. Nas Trimestrais, eles conhecem os projetos destinados aos próximos meses e são desafiados a implantá-los em suas igrejas.&nbsp;Neste quadriênio, tivemos <strong>53 </strong>Encontros Trimestrais presenciais e <strong>2 </strong>virtuais com a participação dos pais.</p>
        <p>Os professores recebem, no início dos trimestres, auxiliares referentes a cada sala, do Rol aos Adolescentes, com o objetivo de apoiá-los e orientá-los com as atividades do trimestre, com conteúdos relacionados a cada faixa etária de suas crianças.&nbsp;Estes materiais são entregues às igrejas sem custo algum.</p>
        <p>Dentro do processo de Comunhão, Relacionamento e Missão, alguns projetos foram realizados neste quadriênio:</p>
        <p>Semana Santa nos lares</p>
        <p>ECF - Crianças no Cozinha</p>
        <p><strong>Treinamentos</strong></p>
        <p>O Ministério da Criança treinou e capacitou coordenadores e professores dos distritos e igrejas locais ao longo do quadriênio.</p>
        <p><strong>2017</strong></p>
        <p>Treinamento para todos os <strong>43 </strong><strong>d</strong><strong>istritos</strong>/ <strong>221 congregações</strong></p>
        <ul>
        <li>Contadores de História</li>
        <li>Guardiões dos Tesouros</li>
        <li>Reparando Brechas</li>
        </ul>
        <p><strong>2018</strong></p>
        <ul>
        <li>Elo da Graça<strong>&nbsp;</strong>(coordenadores, professores e distritais) – <strong>364 </strong>participantes (3 regiões)</li>
        <li>Treinamento Integrado Catre Purunã<strong>&nbsp;</strong><strong>– </strong><strong>43</strong>&nbsp;distritos representados</li>
        </ul>
        <p><strong>2019</strong></p>
        <ul>
        <li>Eu Conheço a Minha História (história da IASD)</li>
        <li>Nas Cores, Uma Linda História (plano de marcar a Bíblia)</li>
        <li>Guardiões dos Tesouros (Mordomia Infantil)</li>
        <li>Treinamento Integrado Catre Purunã para distritais<strong> – 42</strong> participantes</li>
        </ul>
        <p><strong>2020</strong></p>
        <ul>
        <li>Treinamento Integrado Catre Purunã para distritais<strong> – 42</strong> participantes</li>
        <li>Treinamento online para pais, professores, coordenadores e distritais</li>
        </ul>
        <p>Contadores de Histórias &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Eu Conheço Minha História</p>
        <p>Líderes Distritais Curitiba e Interior</p>
        <p>Novas gerações mostrando aos líderes que querem ser parte ativa nas igrejas.</p>
        <p><strong>Ministério do Adolescente</strong></p>
        <p>Neste quadriênio, nossas energias centralizaram-se no crescimento e desenvolvimento da Geração148Teen. Estabelecemos na ACP 111 bases. Hoje, representam o melhor meio de interação e envolvimento dos adolescentes.&nbsp;O objetivo é levá-los a uma melhor comunhão diária com Jesus e&nbsp;participação nas atividades na igreja, fortalecendo-os&nbsp;nos relacionamentos, e levando-os a testemunharem de Jesus nos lugares onde estão inseridos. Nosso lema: “...quer vivamos ou morramos somos do Senhor!” (Romanos 14:8).</p>
        <p>Com o desafio de uma nova geração que já nasce conectada às redes sociais e tem sua atenção voltada a tudo o que a mídia oferece, precisamos levá-los a se conectarem com seu Criador.&nbsp;Por isso, capacitamos nossos líderes a terem uma maior interação e preparo ao atenderem nossos adolescentes.&nbsp;</p>
        <p>Em 2018, oferecemos um Curso de Líder, para coordenadores, professores e distritais das três regiões, com <strong>117</strong> participantes.&nbsp;E em 2019, lançamos o Curso de Líderes Online com certificação pelo MEC.</p>
        <p>Envolvendo nossos adolescentes no processo de Comunhão, Relacionamento e Missão, tivemos vários projetos neste quadriênio com grandes resultados:</p>
        <p>Distritais de Curitiba e Interior</p>
        <p>Vimos um crescimento surpreendente de bases, onde nossos adolescentes se encontram a cada sábado na Escola Sabatina. O estudo da lição, dos livros do Espírito de Profecia, sociabilidade&nbsp;e preparo para os desafios de Comunhão, Relacionamento e Missão, são propostos a cada mês.</p>
        <p><strong>Encontros de adolescentes</strong></p>
        <p>O Ministério do Adolescente realizou em 2017 o Encontro de Meninos – Incomum – com <strong>137 </strong>inscritos. Em 2019<strong>, </strong>o congresso Sou Livre reuniu&nbsp;<strong>634</strong> inscritos.</p>
        <p>Durante o ano,&nbsp;nossos adolescentes&nbsp;cumprem desafios, realizam atividades&nbsp;com intuito de serem melhores&nbsp;à sociedade, ao próximo e a Cristo. No final do ano, eles participam do CelebraTeen, uma grande celebração das conquistas e vitórias pelos desafios cumpridos.</p>
        <ul>
        <li>CelebraTeen&nbsp;2017&nbsp;–&nbsp;<strong>450</strong> inscritos</li>
        <li>CelebraTeen 2018 – <strong>670</strong> inscritos</li>
        </ul>
        <p>Batismo e decisões neste dia de celebração em 2018</p>
        <ul>
        <li>CelebraTeen – 2019 – <strong>780</strong> inscritos</li>
        </ul>
        <p>Grupo de Ukulelê – abrindo a celebração de 2019</p>
        <p>Entrega dos prêmios aos vencedores do Concurso BepTeen (Bom do Espírito de Profecia), estudo dos livros da Série Conflito, conjugados à Lição da Escola Sabatina. Lançado em 2019, com provas mensais.</p>
        <p>Base Light House (Igreja do Vista Alegre – Curitiba) vencedora do prêmio da DSA, como melhor base da União Sul no concurso BepTeen 2019.</p>
        <p><strong>Agradecimentos</strong></p>
        <p>Nossa gratidão a Deus, aos administradores, aos pais e líderes, pelo apoio e dedicação a este ministério que tanto presa e se preocupa em levar esta nova geração a se consagrar e se preparar para viver eternamente com o Senhor.</p>
        <p>Não posso deixar de agradecer aos professores, coordenadores, distritais, regionais e nossos queridos pastores distritais. Muito se empenharam, dedicando tempo e carinho para estarem com esses meninos e meninas em todas as atividades e projetos que são propostos para este ministério.</p>
    </body>
</html>