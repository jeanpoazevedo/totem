<?php

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title>Quadrienal</title>

        <link href="assets/plugins/custombox/css/custombox.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>

    <body>
        <div class="wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title"> </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title"> </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title"> </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0"><b>Líderes Administrativos</b></h4>
                            <p class="text-muted m-b-30 font-13">
                            </p>

                            <div id="full-width-modal-1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da presidência da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r1.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da secretaria da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r2.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da tesouraria e expansão patrimonial da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r3.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="button-list">
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-1">
                                            <img src="arquivos/Lourival_Gomes.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600 text-center">
                                            Pr. Lourival Gomes
                                        </h4>
                                        <p class="text-muted m-b-30 font-13 text-center">
                                            Presidente
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-2">
                                            <img src="arquivos/Paulo_Machado.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Pr. Paulo Machado
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Secretário
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-3">
                                            <img src="arquivos/Ilton_Hubner.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Pr. Ilton César Hübner
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Tesoureiro
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0"><b>Departamentais</b></h4>
                            <p class="text-muted m-b-30 font-13">
                            </p>

                            <div id="full-width-modal-4" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da educação e liberdade religiosa da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r4.php';
                                            ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-5" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da escola sabatina, ministério pessoal e ASA da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r5.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-6" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da associação ministerial, ministério da família e ministério da saúde da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r6.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-7" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da evangelismo e mordomia da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r7.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-8" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da ministério jovem, música e comunicação da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r8.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-9" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da ministério de desbravadores e aventureiros da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r9.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-10" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da ministério da mulher e recepção da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r10.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-11" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da ministério da criança e ministério do adolescente da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r11.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-12" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da área feminina da associação ministerial (</strong><strong>Afam</strong><strong>) da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r12.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="full-width-modal-13" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-full">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="full-width-modalLabel">Relatório da ministério de publicações e espírito de profecia da Associação Central Paranaense</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php
                                                include 'r13.php';
                                            ?> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal">Voltar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="button-list">
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-4">
                                            <img src="arquivos/Pedro_Renato.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600 text-center">
                                            Pedro Renato Frozza
                                        </h4>
                                        <p class="text-muted m-b-30 font-13 text-center">
                                            Educação / Liberdade Religiosa
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-5">
                                            <img src="arquivos/Julio_Padilha.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Júlio Padilha
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Escola Sabatina / ASA / Mipes 
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-6">
                                            <img src="arquivos/Cesar_Bilinski.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            César Bilinski
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Lar e Família / Ministerial
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-7">
                                            <img src="arquivos/Julio_Diniz.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Julio Diniz
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Evangelismo
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-8">
                                            <img src="arquivos/Tiago_Santos.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Tiago Santos
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Jovens/ Comunicação/ Música
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-9">
                                            <img src="arquivos/Rogerio_Almeida.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Rogério Almeida
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            MDA
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-10">
                                            <img src="arquivos/Rael_Braz.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Rael Braz
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Ministério da Mulher / Recepção
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-11">
                                            <img src="arquivos/Rita_Assis.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Rita Assis
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Crianças e Adolescentes
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-12">
                                            <img src="arquivos/Viviane_Bilinski.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Viviane Bilinski
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            AFAM
                                        </p>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-2 profile-detail">
                                        <a data-toggle="modal" data-target="#full-width-modal-13">
                                            <img src="arquivos/Gilmar_Anjos.jpg" class="img-circle" alt="profile-image">
                                        </a>
                                        <h4 class="text-uppercase font-600">
                                            Gilmar dos Anjos
                                        </h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Publicações / Espírito de Profecia
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            var resizefunc = [];
        </script>

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>


        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script src="assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="assets/plugins/custombox/js/legacy.min.js"></script>
        
        <script src="assets/plugins/flot-chart/jquery.flot.min.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.time.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.resize.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.pie.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.selection.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.stack.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.orderBars.min.js"></script>
        <script src="assets/plugins/flot-chart/jquery.flot.crosshair.js"></script>
        <script src="assets/pages/jquery.flot.init.js"></script>
	
    </body>
</html>