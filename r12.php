<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Viviane Dos Santos </strong><strong>Bilinski</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Licenciatura em Música – Escola de Música e Belas Artes do Paraná (2002).</li>
        <li>Pós graduação em Aconselhamento Familiar – Sociedade Paranaense de Ensino e Informática - SPEI (2020).</li>
        <li>Psicologia – Centro Universitário UniDomBosco (em curso).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Pianista Acompanhante – Centro Cultural Guaíra - Curitiba (1999&nbsp;-&nbsp;2013).</li>
        <li>Professora de Piano (1998-2020).</li>
        <li>Diretora de Departamento – Associação Central Paranaense - ACP (Novembro 2019).</li>
        </ul>
        <p>A Afam tem como objetivo apoiar o grupo de esposas de pastores e anciãos de igrejas e incentivar cada uma a desenvolver sua função como esposa de ministro, mãe, conselheira, irmã, mulher e amiga na fé, para se tornarem uma bênção nos locais onde atuam.</p>
        <p><strong>M</strong><strong>ulheres</strong><strong> comprometidas com o lar</strong><strong> e o bem estar</strong><strong> da família</strong></p>
        <p>No projeto&nbsp;<strong>Guardiãs do Lar</strong>,&nbsp;as mães participam de uma jornada de oração, instruídas de que educar é um alto privilégio, mas também uma enorme responsabilidade. As esposas de pastores participaram e&nbsp;replicaram às mães de suas igrejas.<strong>&nbsp;</strong></p>
        <p>A saúde e o bem estar foram promovidos pelo projeto<strong>&nbsp;</strong><strong>Afam</strong><strong> Fit</strong><strong>.</strong><strong>&nbsp;</strong>Idealizado especialmente para as esposas de pastores, para futuramente ser replicado nas igrejas, o foco&nbsp;são os benefícios dos oito remédios naturais. São 40 dias de mudanças de hábitos, contando com a ajuda e suporte de profissionais de Educação Física e Nutrição.&nbsp;</p>
        <p><strong>Encontros dos últimos quatro anos</strong></p>
        <p>As esposas de pastores ordenados e de aspirantes&nbsp;participaram de<strong> Encontros Regionais Trimestrais</strong>, nos quais&nbsp;foram apresentados os projetos do ano, comemorados os aniversários e oferecido suporte para o desenvolvimento da família pastoral e seu ministério.</p>
        <p>Foi realizado também o concílio com o tema<strong> Esposa com Paixão</strong><strong>, </strong>com os oradores:&nbsp;pastor Lucas Alves (Secretário Ministerial Associado da DSA), que falou sobre&nbsp;Paixão pelo Esposo, Paixão por Deus e pela Igreja e Paixão pela Missão; professora Nerisangela Alves (DSA), com&nbsp;a mensagem Paixão pela Família; psicóloga Marisa Wolf Lil, que tratou dos assuntos Como Lidar com as Emoções, Desafios e Ansiedade; professora Danielly Palmeira, com Curso de Culinária – Leites Vegetais; e Márcia Marinho, Curso de Culinária – Pastéis e Pães.</p>
        <p>No encontro da&nbsp;Afam de Curitiba e região metropolitana<strong>,</strong><strong>&nbsp;</strong>conversamos sobre “A Esposa do Pastor e Sua Identidade na Atualidade”. A mensagem foi proferida pela esposa de pastor&nbsp;Danielly Palmeira, compartilhando sua experiência pessoal, dificuldades e vitórias em ser mulher, esposa, mãe e profissional nos tempos atuais.</p>
        <p>Já&nbsp;anciãos e esposas<strong>&nbsp;</strong>das<strong>&nbsp;</strong>igrejas de Curitiba e região metropolitana participaram<strong>&nbsp;</strong>de um encontro sobre&nbsp;reavivamento e reforma. A importância do culto familiar foi abordada nesse evento. Houve ainda um encontro somente com as esposas de anciãos dessa região.&nbsp;</p>
        <p>A necessidade de submissão e dependência de Deus ao longo da vida e do ministério pastoral foram as ênfases do encontro com os pastores aspirantes e esposas.&nbsp;O objetivo foi preparar e orientar os pastores sobre a conduta pastoral diante das adversidades do ministério, bem como apoiar a família pastoral em eventuais dificuldades.</p>
        <p>Em tempos de distanciamento, realizamos o<strong>&nbsp;</strong><strong>Encontro Virtual Esposa de Ancião</strong><strong>.</strong><strong>&nbsp;</strong>A terapeuta familiar e&nbsp;esposa de pastor Dilene&nbsp;Ebinger, trouxe informações muito pertinentes a respeito da depressão. Conversamos sobre os sintomas, sobre a busca por ajuda profissional e orientações para os casos que necessitam de medicação. Tivemos a presença virtual de mais de 200 pessoas.</p>
        <p>A Afam<strong>&nbsp;</strong>teve ainda outro encontro on-line, intitulado “Como a Psicologia pode Ajudar a Desenvolver Seu Ministério e Potencialidades Individuais”.&nbsp;Nessa reunião, três psicólogas trataram sobre as diferenças de atuação&nbsp;do ministério e a psicologia, inclusive sobre como orientar os membros da igreja em situações diversas de ansiedade,&nbsp;depressão, uso de medicamentos, abuso e violência doméstica.</p>
        <p><strong>Agradecimentos</strong></p>
        <p>Primeiramente a Deus pela oportunidade de trabalhar em Sua obra e assim contribuir para apressar a volta de Jesus.</p>
        <p>Aos administradores do ACP, que sempre apoiaram os projetos deste ministério, aos pastores e esposas que com seu dinamismo participaram dos eventos propostos desde que assumimos.</p>
        <p>Aos funcionários que estavam dispostos a ajudar no que fosse preciso, e a USB, pelas orientações e suporte nos projetos realizados.</p>
        <p>À professora Hilda Erfurth, muito obrigada por liderar esse ministério nos anos de 2014 a 2019.</p>
        <p>Minha gratidão de todo o coração, dizendo: “Até aqui nos ajudou Senhor, por isso estamos alegres”. Sem você, nada teríamos realizado.</p>
    </body>
</html>