<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Ilton César Hübner</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li><span></span>Bacharel em Ciências Contábeis – Sociedade Paranaense de Ensino e Informática - SPEI - Curitiba-PR (2002).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li><span></span>Contador – Associação Norte Paranaense - ANP (1992 a 1995).</li>
        <li><span></span>Tesoureiro Assistente – Associação Sul Paranaense - ASP (1998 a 2006).</li>
        <li><span></span>Administrador Financeiro – Produtos Alimentícios Superbom - SP (2007 a 2010).</li>
        <li><span></span>Tesoureiro – Associação Sul de Rondônia - ASuR (2011 a 2013).</li>
        <li><span></span>Tesoureiro Campo – Associação Central Amazonas - ACeAM (2014 a 2017).</li>
        <li><span></span>Tesoureiro – Associação Central Paranaense - ACP (2018 a 2020).</li>
        </ul>
        <p><strong>Tesouraria</strong></p>
        <p>“O próprio Deus deu origem aos planos para o avanço de Sua obra e tem proporcionado a Seu povo um excesso de meios, a fim de que, quando Ele pedir auxílio, alegremente possam atender. Se forem fiéis em levar para o Seu tesouro os meios que lhes foram emprestados, Sua obra fará rápido progresso, muitas almas serão ganhas para a verdade, e o dia da vinda de Cristo será apresentado!” (Administração Eficaz, p. 45).</p>
        <p>Louvamos a Deus pelas incontáveis bênçãos, pois sabemos que mesmo nosso país passando por alguns desafios econômicos – crescimento baixo, aumentos dos preços generalizados, câmbio flutuante, taxa de desemprego em alta – Deus cuidou para que Sua Igreja permanecesse crescendo ano a ano, contrariando a lógica financeira e a realidade brasileira. Esses fatos podem ser constatados nos balanços demonstrativos de entradas e saídas, bem como em alguns gráficos.</p>
        <p>Dados Financeiros - Associação</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>Crescimento de dízimo no quadriênio</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>R$ 25.215.742,00</td>
                            <td>26,17%</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>R$ 23.924.691,00</td>
                            <td>-5,12%</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>R$ 24.382.933,00</td>
                            <td>1,92%</td>
                        </tr>
                        <tr>
                            <th scope="row">2020*</th>
                            <td>R$ 24.462.665,00*</td>
                            <td>0,34%</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Valor Projetado para 31/12/2020</p>
        <p><strong>Investimento na pregação do Evangelho</strong></p>
        <p>Os recursos administrados pela Associação foram investidos na missão de pregar o Evangelho, e assim levar a Salvação às pessoas. No quadriênio, foram destinados mais de R$ 6,6 milhões ao evangelismo.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>Investimentos em evangelismo e materiais</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>R$ 2.438.554,00</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>R$ 1.390.115,00</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>R$ 1.504.657,00</td>
                        </tr>
                        <tr>
                            <th scope="row">2020*</th>
                            <td>R$ 1.275.989,00*</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Valor Projetado para 31/12/2020</p>
        <p><strong>Auxílio a igrejas e grupos</strong></p>
        <p>Todo dom perfeito que recebemos vem das mãos de Deus. Quando somos redimidos pelo precioso sangue de Cristo, nós O reconhecemos como Senhor dos nossos recursos, pois todos provieram unicamente dEle. Então somos mordomos e nos identificamos com o Mestre, cujos bens nos foi dado supervisionar. Neste quadriênio, muitos irmãos demostraram-se fiéis, permitindo-nos apoiar muitas igrejas. Foram investidos mais de R$ 3,7 milhões para auxiliar igrejas e grupos.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>Auxílio às igrejas</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>R$ 963.231,00</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>R$ 1.146.547,79</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>R$ 1.119.949,09</td>
                        </tr>
                        <tr>
                            <th scope="row">2020*</th>
                            <td>R$ 760.876,00*</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Valor Projetado para 31/12/2020</p>
        <p><strong>Educação Adventista</strong></p>
        <p>Neste quadriênio, muitos sonhos foram realizados. Construções erguidas, ampliações e reformas em várias de nossas escolas mostram que Deus tem nos abençoado a cada dia. Também foi possível ver a mão do Mestre no esforço de centenas de pessoas que desenvolvem o seu ministério na Educação Adventista. Tivemos a alegria de adquirirmos novos terrenos para as escolas adventistas, totalizando o valor de R$ 5.850.000,00.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>Investimentos na Educação</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>R$ 573.917,00</td>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>R$ 3.083.430,79</td>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>R$ 4.686.605,09</td>
                        </tr>
                        <tr>
                            <th scope="row">2020*</th>
                            <td>R$ 2.313.061,00*</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Valor Projetado para 31/12/2020</p>
        <p><strong>SELS</strong></p>
        <p>O quadriênio foi marcado pela reestruturação na área de publicações da ACP. Os índices financeiros foram mantidos, as vendas incrementadas e a cada ano mais servos de Deus dedicam sua vida ao ministério da página impressa. A evangelização também faz parte deste departamento e a ACP tem apoiado a pregação por meio de nossos livros, repassando 1% do dízimo para que possamos apressar a volta de Jesus nesta terra. O valor do repasse nos últimos quatro anos foi de R$ 1.057.618,34.</p>
        <p><strong>Expansão Patrimonial</strong></p>
        <p>Deus é o dono de tudo, somos apenas mordomos a quem Ele confia a administração destes bens. Se formos fiéis, Ele promete “abrir as janelas do céu” e “derramar bênçãos sem medida”, conforme Malaquias 3:10. Impossível seria enumerar todas as bênçãos de Deus para Sua Igreja quanto ao crescimento patrimonial. Tivemos, neste quadriênio, a alegria de adquirir novos terrenos para construção de igrejas, reformas de igrejas e escolas que, conjuntamente, formam um grande patrimônio.</p>
        <p>Deixo aqui registrado o esforço e generosidade de muitos irmãos e irmãs fiéis, bem como pastores distritais dedicados que, aliados à administração da ACP, juntaram os esforços e ampliaram os bens administrados pela Igreja. Deus seja louvado! &nbsp;</p>
        <p><strong>Agradecimentos</strong></p>
        <p>“Que darei eu ao Senhor por todos os Seus benefícios para comigo?” &nbsp;(Salmos 116:12).</p>
        <p>Não há palavras que expressem com exatidão a plenitude das realizações de Deus na vida de Sua Igreja e na vida de cada um de nós. Ao vermos o quanto o Senhor tem feito progredir Sua obra na ACP, enchemo-nos de gratidão e de reconhecimento a Ele. Lembramos de todos aqueles que estiveram ao nosso lado do campo local, ao pastor Lourival, pastor Paulo Machado, departamentais, pastores distritais, pelo apoio recebido.</p>
        <p>Agradecemos à nossa equipe do escritório: tesoureiros assistentes, contadores, auxiliares, secretárias; sem a disposição e sem as incontáveis horas de trabalho desse grupo não seria possível a realização deste trabalho. Nossa gratidão aos administradores e aos demais colaboradores pelo empenho no trabalho em nossas escolas. Aos colportores, nosso apreço e reconhecimento pelas milhares de páginas distribuídas neste período.</p>
        <p>Também agradecemos a cada membro de igreja da ACP, pela confiança e pela fidelidade à causa de Deus e, em especial, agradecemos aos tesoureiros que voluntariamente fazem esse trabalho peculiar. Cada recibo, cada nota, tudo isso contribui para o crescimento da obra de Deus. Que a bênção de Deus repouse sobre todos!</p>
        <p>“Nada temos que recear quanto ao futuro, a menos que esqueçamos a maneira em que o Senhor nos tem guiado, e os ensinos que nos ministrou no passado.” (Mensagens Escolhidas, vol. 3, p. 162).</p>
        <p><strong>Metas para o quadriênio</strong></p>
        <p>1 – Manter o capital operativo e nossos balanços nos índices recomendados pela Igreja.</p>
        <p>2 – Ter 90% do movimento financeiro da ACP informatizados nas igrejas.</p>
        <p>3 – Continuar o processo de modernização nas escolas adventistas.</p>
        <p>4 – Treinar todos os anos a equipe da tesouraria das igrejas.</p>
        <p>5 – Realizar treinamentos regionais aos tesoureiros.</p>
        <p>6 – Apoiar e incentivar a área de Mordomia.</p>
        <p>7 – Participar e apoiar os concílios de Publicações.</p>
        <p>“Há vida na união, um poder que se não pode obter por nenhum outro modo.” (Ellen White, Meditação Matinal, 1956, p. 286).</p>
    </body>
</html>