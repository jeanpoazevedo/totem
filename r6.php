<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>César </strong><strong>Bilinski</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Graduação em Teologia – Centro Universitário Adventista de São Paulo - Unasp (2000).</li>
        <li>Mestrado em Teologia Pastoral – Centro Universitário Adventista de São Paulo - Unasp (2016).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Pastor distrital – Associação Sul Paranaense - ASP (2001-2009).</li>
        <li>Pastor distrital – Associação Central Paranaense - ACP (2010-2019).</li>
        <li>Diretor de Departamento – Associação Central Paranaense - ACP (2020).</li>
        </ul>
        <p><strong>Associação Ministerial</strong></p>
        <p>“Dar-vos-ei pastores segundo o meu coração, que vos apascentem com conhecimento e com inteligência.” (Jeremias 3:15).</p>
        <p>A Secretaria Ministerial tem o propósito de acompanhar e auxiliar os pastores e suas famílias, assim como os anciãos e diáconos das igrejas locais,&nbsp;levando-os a refletirem o caráter de Jesus Cristo, motivando-os a exercerem os métodos do Mestre e colocando ao seu dispor as ferramentas necessárias para este fim.</p>
        <p><strong>Encontros e concílios da Associação Ministerial</strong><strong>, anciãos e diáconos</strong></p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r6/1.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Com o objetivo de inspirar, motivar e capacitar e reavivar espiritualmente os pastores para o desempenho de um ministério mais eficiente, os concílios têm sido o ponto marcante – foram realizados quatro no período. A educação contínua sempre presente, oferecendo ferramentas para capacitação teológica, psicológica, discipulado e pastoreio.</p>
        <p>Além dos concílios, foram realizadas reuniões periódicas com os pastores, Programa de Assistência Mensal aos Pastores (PAMPs).</p>
        <p>No ano de 2018,&nbsp;tivemos o <strong>Concílio para </strong><strong>F</strong><strong>amília </strong><strong>P</strong><strong>astora</strong><strong>l</strong> com a presença de pessoas capacitadas da área de saúde, psicologia e espiritual.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r6/2.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Nestes quatro anos, todos os pastores distritais, capelães, administradores e departamentais foram visitados. O propósito foi ouvi-los, ajudá-los e orientá-los, bem como orar pela família e com a família.&nbsp;Total de 208 visitas no quadriênio.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r6/3.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Compreendendo que o ancião e o diretor de grupo precisam pastorear a igreja, a ACP investiu alto na formação desses líderes, mesmo em momento de pandemia. Dentro da ênfase para Comunhão, Relacionamento e Missão, procuramos motivar e inspirar nossos líderes para que continuem dedicando-se e consagrando-se a esta causa tão nobre.</p>
        <p>Nos anos de 2017 e 2018,&nbsp;concílios na capital e interior. No de 2020, em formato de live para o Dia do Ancião e treinamentos. Abordamos assuntos relevantes: discipulado, Cada Um Salvando Um, temas doutrinários, administração de igreja, história da Igreja e abordagem teológica. Foram distribuídos materiais como a Revista do Ancião, o Manual da Igreja e o Guia do Ancião. Agradecemos aos nossos anciãos e diretores que participaram nestes quatro anos.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r6/4.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>A igreja tem gratidão para com os diáconos e diaconisas, que dedicam muito de seu tempo, energia e esforços na obra de “servir”. Neste quadriênio, tivemos treinamentos para os líderes distritais deste ministério, sempre abordado a importância e privilégio de servir. Neste ano de 2020, foi dado a ênfase na Roda do Discipulado em atender a igreja local e a comunidade externa.</p>
        <p><strong>Ordenação</strong><strong> de </strong><strong>p</strong><strong>astores</strong><strong> no quadriênio</strong></p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r6/5.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r6/6.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Os pastores aspirantes ao ministério tiveram o apoio tanto individual como coletivo em encontros para o bom desempenho de seu ministério.&nbsp;Nesses quatro anos, testemunhamos com alegria a ordenação dos seguintes pastores ao sagrado ministério:&nbsp;Marcelo Ferreira Branco (2017), Rogério de Almeida (2018), Hugo Oliveira da Silva (2019), Luiz Francisco Ferreira Júnior (2019), Paulo Cesar Ribeiro (2020) e Gilmar dos Anjos&nbsp;(2020).</p>
        <p><strong>Ministério da Família</strong></p>
        <p>A família foi estabelecida por criação divina como a instituição humana fundamental. Este ministério a fortalece como centro do discipulado, visando&nbsp;as necessidades de casais, pais e filhos, ao passarem pelas etapas previsíveis da vida e lutarem com inesperadas alterações em sua existência.&nbsp;Enfatiza&nbsp;também a importância da família para o cumprimento da missão, o papel dos pais no processo de discípulos e sua capacidade de influenciar as gerações seguintes.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r6/7.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Visando à orientação dos jovens casais em diferentes áreas do casamento, a ACP ofereceu sete cursos de noivos, abordando o espiritual, comunicação do casal, sexualidade, como lidar com as diferenças e saúde financeira. Os casais também receberam instruções sobre o cerimonial do casamento. O evento contou com o apoio de pastores, médicos e psicólogos experientes na área de família.&nbsp;Tivemos a presença de 116 casais de noivos durante o quadriênio.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r6/8.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>“Pois toda a casa é estabelecida por alguém, mas aquele que estabeleceu todas as coisas é Deus.” (Hebreus 3:4).</p>
        <p>Durante o quadriênio, foram realizados 113&nbsp;<strong>Encontro</strong><strong>s</strong><strong> de Casais</strong>, contando com a direção da ACP, como também das igrejas locais.&nbsp;O departamento do Ministério da Família apoiou e orientou aos distritos para realizaram este encontro tão importante para os nossos dias.</p>
        <p>Todos os distritos se envolveram com o projeto&nbsp;<strong>Adoração em Família</strong>, que consiste na leitura e estudo de algum livro do Espírito de Profecia. Por meio da distribuição destes livros, buscou-se fortalecer as famílias.</p>
        <p>Em 2017, foi estudado o livro Mente Caráter e Personalidade, volume 2. Em 2018,&nbsp;Fundamentos do Lar Cristão.&nbsp;Em 2019, o livro estudado foi Bênçãos sem Medidas. E,&nbsp;finalizando com o ano de 2020, o livro Coração da Igreja.&nbsp;Com isto, chegamos a um total de 211 igrejas envolvidas neste projeto.</p>
        <p>Livros Adoração em Família</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r6/9.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r6/10.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>Este ministério tem se preocupado ano após ano com a <strong>Semana de Oração da Família</strong>, programa tão importante para as famílias da igreja.&nbsp;Por isso, tem providenciado os materiais necessários para que, no mês sugerido aconteça em todas as igrejas e grupos.&nbsp;</p>
        <p>Enfatizando a necessidade de cada família para vivenciar a Comunhão, Relacionamento e a Missão, neste quadriênio foram realizadas 162 semanas de oração.</p>
        <p>“Uma família bem ordenada, bem disciplinada, fala mais em favor do cristianismo do que todos os sermões que se possa pregar” (Lar Adventista, p.32).</p>
        <p><strong>Ministério </strong><strong>da </strong><strong>Saúde</strong></p>
        <p>O Ministério da Saúde procurou levar os membros da ACP à Comunhão, fortalecendo a mensagem de saúde como marca distintiva; ao Relacionamento, envolvendo a igreja no compartilhamento de um estilo de vida saudável; e à Missão, consolidando um modelo de evangelismo da saúde.</p>
        <p>Destacamos envolvimento da igreja com o total de 221 pessoas no curso de <strong>Culinária Saudável</strong>, 297 no projeto <strong>Mexa-se pela Vida</strong> e 201 no curso&nbsp;<strong>C</strong><strong>omo Deixar de Fumar</strong>.</p>
        <div class="row">
            <div class="col-sm-6">
                <img src="r6/11.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-6">
                <img src="r6/12.png" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <p>A<strong>&nbsp;</strong><strong>Feira Vida e Saúde</strong>&nbsp;visa dois objetivos principais, que são levar os membros de nossa igreja a uma conscientização quanto à reforma de saúde através dos oito remédios naturais, como também um grande instrumento para despertar interesse na comunidade para conhecer nossa mensagem e filosofia de vida, tendo portanto um cunho evangelístico. Contamos com uma equipe muito especial de voluntários para realização deste projeto. Entre eles, médicos, enfermeiros, fisioterapeutas, membros leigos, e o grande apoio dos&nbsp;calebes&nbsp;e G148.&nbsp;Neste período, tivemos 285 feiras realizadas em nosso território e total de 30.940 pessoas atendidas.</p>
        <p><strong>Agradecimentos</strong></p>
        <p>Queremos louvar a Deus pelas bênçãos que Ele tem dado à sua Igreja e também ao ministério que nos foi confiado. Agradecemos ao pastor&nbsp;Gunter&nbsp;Erfurth,&nbsp;que liderou a Associação Ministerial e o Ministério da Família até o ano de 2019, e o Ministério da Saúde no ano de 2019. Agradecemos também ao pastor André Flores, que liderou o Ministério da Saúde de 2017 a 2018.</p>
    </body>
</html>