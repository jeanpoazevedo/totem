<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Paulo Machado</strong></p>
        <p><strong>“Até aqui nos ajudou o Senhor por isso estamos alegres” Salmos 126:3</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li><span></span>Graduação em Teologia – Centro Universitário Adventista de São Paulo - Unasp (1999).</li>
        <li><span></span>Pós-Graduação em Teologia – Faculdade Teológica Sul Americana - Londrina (2006).</li>
        <li><span></span>Mestrado em Liderança – Andrews University - Berrien Springs, Michigan (2016).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li><span></span>Pastor escolar – Associação Catarinense - AC (2000 a 2002).</li>
        <li><span></span>Pastor distrital – Associação Catarinense - AC (200 a 2007).</li>
        <li><span></span>Diretor de Departamento – Associação Mineira Sul - AMS (2008 a 2011).</li>
        <li><span></span>Secretário Executivo – Associação Central Paranaense - ACP (2012 a 2020).</li>
        </ul>
        <p>A Secretaria da Associação Central Paranaense ocupa um espaço importante na estrutura administrativa da Igreja, pois constitui um canal por onde flui as informações da vida da mesma, com reflexos internos e externos. Para isso, fizemos um compromisso com as áreas administrativa, técnica e missionária.&nbsp;&nbsp;Adotamos comportamentos de celeridade e fidelidade com informações atualizadas e confiáveis nos registros, sempre respeitando as deliberações através de votos, procedimentos do Manual da Igreja e no Livro de Regulamentos Eclesiástico-Administrativos (REA).</p>
        <p>Com objetivo de atender às necessidades da Igreja no discipulado e pastoreio, foi utilizado o projeto <strong>Secretaria Nota 1.000</strong>, que são ações de excelência para execução do trabalho.</p>
        <p>Foi com essa consciência que não medimos esforços em investir em capacitação através de encontros distritais, regionais, e no atendimento personalizado, sempre procurando inspirar, valorizar e capacitar os secretários de igrejas e grupos que fizeram deste projeto sua prioridade.</p>
        <p>Por isso, temos alegria de apresentar à Igreja este relatório estatístico, demonstrativo e visual, que revela o desenvolvimento alcançado durante os anos de 2017 a 2020.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r2/1.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <img src="r2/2.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>&nbsp;</p>
        <p><strong>Faixa etária</strong></p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r2/3.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>&nbsp;</p>
        <p><strong>Estado Civil</strong></p>
        <div class="row">
            <div class="col-sm-9">
                <img src="r2/4.png" alt="image" class="img-responsive img-rounded"/>
            </div>
            <div class="col-sm-3">
                <img src="r2/5.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b><strong>Entradas</strong>: Batismos, Rebatismo e Profissão de Fé</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Entradas</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020*</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Batismo</th>
                            <td>1.626</td>
                            <td>1.014</td>
                            <td>1.330</td>
                            <td>203</td>
                            <th>4.173</th>
                        </tr>
                        <tr>
                            <th scope="row">Rebatismo</th>
                            <td>432</td>
                            <td>249</td>
                            <td>327</td>
                            <td>78</td>
                            <th>1.086</th>
                        </tr>
                        <tr>
                            <th scope="row">Profissão de Fé</th>
                            <td>79</td>
                            <td>109</td>
                            <td>104</td>
                            <td>19</td>
                            <th>311</th>
                        </tr>
                        <tr>
                            <th scope="row">Total de Entradas</th>
                            <th>2.137</th>
                            <th>1.372</th>
                            <th>1.761</th>
                            <th>300</th>
                            <th>5.570</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>* Até 30 de setembro 2020</p>
        <p><strong>Modo de Conversão – </strong>Incluir gráfico do modo de conversão</p>
        <p><strong>Saídas</strong>: Apostasia e Desaparecimento</p>
        <p>É com pesar que apresentamos essas perdas. Entretanto, o <strong>Projeto Reencontro</strong>&nbsp;aconteceu anualmente, no qual centenas de pessoas tiveram a oportunidade de voltar para casa do Pai. &nbsp;Isso aconteceu sem esquecer da forte ênfase em discipulado e pastoreio, através da Unidade de Ação da Escola Sabatina e forte plano de visitação, que estão sendo realizados visando a diminuição destes dados no futuro próximo.</p>
        <p>Por isso, o foco da secretaria é ajudar no acompanhamento dos novos membros, matriculando-os nos cartões da Escola Sabatina, acompanhar os membros ativos para que ajudem a conservar o que se tem conquistado e resgatar o que se perdeu.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b><strong>Entradas</strong>: Batismos, Rebatismo e Profissão de Fé</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Remoção</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020*</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Apostasia</th>
                            <td>1.865</td>
                            <td>1.905</td>
                            <td>1.269</td>
                            <td>255</td>
                            <th>5.294</th>
                        </tr>
                        <tr>
                            <th scope="row">Desaparecimento</th>
                            <td>798</td>
                            <td>1.151</td>
                            <td>397</td>
                            <td>48</td>
                            <th>2.394</th>
                        </tr>
                        <tr>
                            <th scope="row">Total de Remoções</th>
                            <th>2.663</th>
                            <th>3.056</th>
                            <th>1.666</th>
                            <th>303</th>
                            <th>7.688</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Até 30 de setembro 2020</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r2/6.jpg" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>&nbsp;</p>
        <p><strong>Classificação dos Membros</strong></p>
        <p>Para ter um discipulado e pastoreio efetivo, temos um sistema organizado e permanente de revisão do Registro de Membros, que constantemente está classificando a realidade da situação de cada membro, especialmente para detectar os nomes dos membros que estão fracos na fé, que necessitam de urgente atenção, para poder ajudá-los.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r2/7.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>Com o crescimento da Igreja e sua liderança, registramos a organização de sete grupos que tiveram seu status alterado para igreja:</p>
        <p><strong>Igrejas Organizadas </strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Número</th>
                            <th>Igreja</th>
                            <th>Distrito</th>
                            <th>Ano da Organização</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Jardim Boa Vista II</td>
                            <td>Jaguariaíva</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Bacacheri</td>
                            <td>Tingui</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Campo Novo I</td>
                            <td>Jaguariaíva</td>
                            <td>2018</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Jardim Boa Vista</td>
                            <td>São Braz</td>
                            <td>2018</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Vila Elisabeth</td>
                            <td>Campo Largo</td>
                            <td>2019</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Jardim Pontagrossense</td>
                            <td>Uvararanas</td>
                            <td>2019</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Tibagi</td>
                            <td>Jardim Alegre</td>
                            <td>2019</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p><strong>Dados Gerais</strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th></th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Municípios</td>
                            <td>82 a conferir</td>
                        </tr>
                        <tr>
                            <td>Distritos</td>
                            <td>43</td>
                        </tr>
                        <tr>
                            <td>Igrejas</td>
                            <td>144</td>
                        </tr>
                        <tr>
                            <td>Grupos</td>
                            <td>74</td>
                        </tr>
                        <tr>
                            <td>Membros</td>
                            <td>23.201</td>
                        </tr>
                        <tr>
                            <td>Pastores ordenados</td>
                            <td>44</td>
                        </tr>
                        <tr>
                            <td>Pastores aspirantes</td>
                            <td>12</td>
                        </tr>
                        <tr>
                            <td>Pastores em Escolas e Colégios</td>
                            <td>9</td>
                        </tr>
                        <tr>
                            <td>Pastores jubilados</td>
                            <td>9</td>
                        </tr>
                        <tr>
                            <td>Colportores Credenciados</td>
                            <td>04</td>
                        </tr>
                        <tr>
                            <td>Colportores Licenciados</td>
                            <td>02</td>
                        </tr>
                        <tr>
                            <td>Colportores Aspirantes</td>
                            <td>05</td>
                        </tr>
                        <tr>
                            <td>Funcionários Escritório</td>
                            <td>93 a confeir</td>
                        </tr>
                        <tr>
                            <td>Funcionários Educação</td>
                            <td>557</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p><strong>Serviço Voluntário Adventista</strong></p>
        <p>A prática do voluntariado é uma tendência que vem crescendo cada vez mais em nossa sociedade, principalmente com as novas gerações. Aliando a necessidade de proclamação do evangelho em lugares desafiadores com tal tendência, o Serviço Voluntário Adventista (SVA) tem como propósito colocar à disposição, de forma organizada e oficial, oportunidades de serviço voluntário temporário para adventistas jovens e adultos, estudantes e profissionais, nas mais vastas e necessitadas regiões do mundo, apoiando a Igreja na proclamação do evangelho.</p>
        <p>Numa parceria com o Ministério Jovem, todos os anos temos formado jovens na Escola de Missões Send Me. Com um currículo teórico prático, nossos missionários têm se desenvolvido e se colocado à disposição para servir a Igreja em todo o mundo. Além das missões transculturais de curta duração, incentivamos nossos missionários a servir durante períodos mais longos em projetos específicos. Neste quadriênio, enviamos jovens em missão com Deus para servir a Igreja no Egito, Líbano, Amazônia.</p>
        <div class="row">
            <div class="col-sm-12">
                <img src="r2/8.png" alt="image" class="img-responsive img-rounded"/>
            </div>
        </div>
        <p>&nbsp;</p>
        <p><strong>Missão Global</strong></p>
        <p>Missão Global é o setor da Igreja responsável por promover e desenvolver projetos que possibilitem o estabelecimento da presença adventista em grupos étnicos, culturais, linguísticos, minoritários, em bairros e municípios onde a Igreja ainda não existe, cumprindo assim, não apenas o alvo numérico, mas também o alvo geográfico e demográfico.</p>
        <p>Para isso, neste quadriênio o esforço foi na formação de plantadores de igreja, com o objetivo primário de cumprir o propósito de Deus em suas vidas e através dos seus dons capacitar membros voluntários e pastores para a expansão do Reino de Deus.</p>
        <p>O método mais eficaz para o avanço do Reino de Deus é o estabelecimento de igrejas com uma prática de ser e fazer igreja centrada no evangelho, na cidade e na missão; e uma reflexão baseada na missão de Deus, na missão da Igreja, no ministério e na vida de Cristo.</p>
        <p><strong>Análise dos municípios da Associação Central Paranaense</strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <tbody>
                        <tr>
                            <td>Municípios</td>
                            <td>82</td>
                        </tr>
                        <tr>
                            <td>População</td>
                            <td>3.366.368</td>
                        </tr>
                        <tr>
                            <td>IDH</td>
                            <td>0,67</td>
                        </tr>
                        <tr>
                            <td>PIB Total</td>
                            <td>122,44 milhões</td>
                        </tr>
                        <tr>
                            <td>Domicílios</td>
                            <td>1.187.394</td>
                        </tr>
                        <tr>
                            <td>Renda Média</td>
                            <td>2.920,41</td>
                        </tr>
                        <tr>
                            <td>Adventistas</td>
                            <td>23.201</td>
                        </tr>
                        <tr>
                            <td>Católicos</td>
                            <td>74,14%</td>
                        </tr>
                        <tr>
                            <td>Evangélicos</td>
                            <td>24,5%</td>
                        </tr>
                        <tr>
                            <td>Habitante por Adventista</td>
                            <td>145</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p><strong>Plantio de igrejas (2017 a 2020)</strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Número</th>
                            <th>Grupo</th>
                            <th>Município</th>
                            <th>Ano de Abertura</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Diadema</td>
                            <td>Curitiba</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jardim Israelense</td>
                            <td>Araucária</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Termas Rivieira</td>
                            <td>Castro</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>São Marcos</td>
                            <td>Bocaiúva do Sul</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Santa Terezinha</td>
                            <td>Curitiba</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Avenida João Franco</td>
                            <td>Contenda</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Dois Mil</td>
                            <td>Guarapuava</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Ipiranga</td>
                            <td>Ipiranga</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Jardim Buenos Aires</td>
                            <td>Almirante Tamandaré</td>
                            <td>2017</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>Costa Rica</td>
                            <td>Ponta Grossa</td>
                            <td>2018</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>Porto Novo</td>
                            <td>Adrianópolis</td>
                            <td>2018</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>Jardim Alvorada</td>
                            <td>Castro</td>
                            <td>2019</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>Km 15</td>
                            <td>Campo Largo</td>
                            <td>2019</td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>Mercês</td>
                            <td>Curitiba</td>
                            <td>2019</td>
                        </tr>
                        <tr>
                            <td>15</td>
                            <td>Cohapar</td>
                            <td>Ibaiti</td>
                            <td>2019</td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>Jardim Bonfim</td>
                            <td>Almirante Tamandaré</td>
                            <td>2019</td>
                        </tr>
                        <tr>
                            <td>17</td>
                            <td>Home Church</td>
                            <td>Castro</td>
                            <td>2019</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p><strong>&nbsp;</strong><strong>Cidades de Missão Global sem nenhuma presença adventista</strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Número</th>
                            <th>Município</th>
                            <th>Distrito</th>
                            <th>População*</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mato Rico</td>
                            <td>Palmital</td>
                            <td>4.205</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>São João do Triunfo</td>
                            <td>Irati</td>
                            <td>14.996</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Dados 2018</p>
        <p><strong>Cidades de Missão Global com famílias isoladas</strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Número</th>
                            <th>Município</th>
                            <th>Distrito</th>
                            <th>População</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Reserva do Iguaçu</td>
                            <td>Laranjeiras do Sul</td>
                            <td>7.815</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Fernandes Pinheiros</td>
                            <td>Irati</td>
                            <td>5.954</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Guamiranga</td>
                            <td>Irati</td>
                            <td>8.484</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Porto Barreiro</td>
                            <td>Laranjeiras do Sul</td>
                            <td>3.564</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Virmond</td>
                            <td>Laranjeiras do Sul</td>
                            <td>4.085</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Goioxim</td>
                            <td>Palmital</td>
                            <td>7.976</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Boa Ventura de São Roque</td>
                            <td>Pitanga</td>
                            <td>6.683</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Nova Tebas</td>
                            <td>Pitanga</td>
                            <td>5.856</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Sapopema</td>
                            <td>Arapoti</td>
                            <td>6.751</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>Japira</td>
                            <td>Quatiguá</td>
                            <td>4.910</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>Jaboti</td>
                            <td>Quatiguá</td>
                            <td>4.895</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>Conselheiro Mairinck</td>
                            <td>Quatiguá</td>
                            <td>3.843</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>Santana do Itararé</td>
                            <td>Wenceslau Braz</td>
                            <td>5.244</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>A despeito de todo o trabalho, olho para trás e percebo que poderíamos fazer mais. Todavia, olho com confiança à frente e vejo um futuro brilhante para a Igreja de Deus. Consigo visualizar milhares de pessoas sendo alcançadas pelo poder do Evangelho, novos lugares sendo alcançados. Vislumbro novas congregações se abrindo, comunidades sendo impactadas, jovens louvando, pessoas sendo batizadas, um povo se preparando para a Volta de Jesus e uma igreja vitoriosa.</p>
    </body>
</html>