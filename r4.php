<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Pedro Renato Frozza</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li><span></span>Licenciatura Plena em Matemática – Universidade Luterana do Brasil - ULBRA (1999).</li>
        <li><span></span>Especialização em Educação – Faculdade Porto-Alegrense - FAPA (2020).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li><span></span>Diretor de Departamento – Educação – Associação Sul Mato-grossense - ASM (2009 – 2010).</li>
        <li><span></span>Diretor de Departamento – Educação – Associação Planalto Central - APLaC (2011 – 2012).</li>
        <li><span></span>Diretor de Departamento – Educação – União Centro-Oeste Brasileira - UCOB (2013 – 2017).</li>
        <li><span></span>Diretor de Departamento – Educação – Associação Central Paranaense - ACP (2018 – 2020).</li>
        </ul>
        <p>Quando Ellen White teve a primeira visão sobre educação dos filhos da igreja, ela citou: “Deus declarou ser desígnio Seu possuir um colégio em que a Bíblia tenha seu devido lugar na educação da juventude” (Testemunho para a Igreja, vol. 5, p. 26).</p>
        <p>Diante de tantas bênçãos recebidas, queremos destacar algumas conquistas da Educação e Liberdade Religiosa. Salientamos o trabalho que o professor Jeferson Elias de Souza desempenhou com competência e organização à frente destes departamentos até agosto de 2018. Muito do que apresentaremos neste relatório é resultado das ações dele e da sua equipe.</p>
        <p><strong>Os números da Educação Adventista da ACP no quadriênio</strong></p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>Número de alunos</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Escolas</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Araucária</th>
                            <td>630</td>
                            <td>816</td>
                            <td>912</td>
                            <td>942</td>
                        </tr>
                        <tr>
                            <th scope="row">Boa Vista</th>
                            <td>861</td>
                            <td>817</td>
                            <td>798</td>
                            <td>845</td>
                        </tr>
                        <tr>
                            <th scope="row">Boqueirão</th>
                            <td>1.156</td>
                            <td>1.172</td>
                            <td>1.178</td>
                            <td>1.123</td>
                        </tr>
                        <tr>
                            <th scope="row">Castro</th>
                            <td>173</td>
                            <td>180</td>
                            <td>165</td>
                            <td>136</td>
                        </tr>
                        <tr>
                            <th scope="row">Guarapuava</th>
                            <td>376</td>
                            <td>414</td>
                            <td>389</td>
                            <td>375</td>
                        </tr>
                        <tr>
                            <th scope="row">Ponta Grossa</th>
                            <td>410</td>
                            <td>451</td>
                            <td>434</td>
                            <td>624</td>
                        </tr>
                        <tr>
                            <th scope="row">Portão</th>
                            <td>1.132</td>
                            <td>1.042</td>
                            <td>1.033</td>
                            <td>1.064</td>
                        </tr>
                        <tr>
                            <th scope="row">Santa Efigênia</th>
                            <td>418</td>
                            <td>434</td>
                            <td>410</td>
                            <td>375</td>
                        </tr>
                        <tr>
                            <th scope="row">Telêmaco Borba</th>
                            <td>474</td>
                            <td>441</td>
                            <td>410</td>
                            <td>371</td>
                        </tr>
                        <tr>
                            <th scope="row">Vista Alegre</th>
                            <td>133</td>
                            <td>262</td>
                            <td>404</td>
                            <td>500</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <th>5.763</th>
                            <th>6.029</th>
                            <th>6.133</th>
                            <th>6.355</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>Receita escolares</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>EI</th>
                            <th>EFI</th>
                            <th>EFII</th>
                            <th>EM</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">2017</th>
                            <td>R$ 3.342.950,71</td>
                            <td>R$ 13.386.482,92</td>
                            <td>R$ 11.783.026,10</td>
                            <td>R$ 5.039.687,81</td>
                            <th>R$ 33.552.147,54</th>
                        </tr>
                        <tr>
                            <th scope="row">2018</th>
                            <td>R$ 4.372.414,43</td>
                            <td>R$ 14.927.899,43</td>
                            <td>R$ 12.885.744,09</td>
                            <td>R$ 5.152.191,23</td>
                            <th>R$ 37.338.249,18</th>
                        </tr>
                        <tr>
                            <th scope="row">2019</th>
                            <td>R$ 5.134.852,42</td>
                            <td>R$ 16.431.544,84</td>
                            <td>R$ 13.990.125,05</td>
                            <td>R$ 5.390.969,49</td>
                            <th>R$ 40.947.491,80</th>
                        </tr>
                        <tr>
                            <th scope="row">2020</th>
                            <td>R$ 3.097.846,58</td>
                            <td>R$ 10.714.193,68</td>
                            <td>R$ 8.787.168,57</td>
                            <td>R$ 3.191.423,84</td>
                            <th>R$ 25.790.632,67*</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Até julho 2020.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <h4 class="m-t-0 header-title"><b>Investimentos</b></h4>
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Escolas</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Araucária</th>
                            <td>R$ 5.727.026,42</td>
                            <td>R$ 1.500.000,00</td>
                            <td>R$ 500.000,00</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Boa Vista</th>
                            <td>-</td>
                            <td>-</td>
                            <td>R$ 149.552,99</td>
                            <td>R$ 55.000,00</td>
                        </tr>
                        <tr>
                            <th scope="row">Boqueirão</th>
                            <td>-</td>
                            <td>-</td>
                            <td>R$ 86.466,11</td>
                            <td>R$ 150.000,00</td>
                        </tr>
                        <tr>
                            <th scope="row">Castro</th>
                            <td>-</td>
                            <td>R$ 15.000,00</td>
                            <td>R$ 26.544,09</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Guarapuava</th>
                            <td>R$ 30.403,40</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Ponta Grossa</th>
                            <td>-</td>
                            <td>R$ 1.500.000,00</td>
                            <td>R$ 6.000.000,00</td>
                            <td>R$ 4.689.245,76</td>
                        </tr>
                        <tr>
                            <th scope="row">Portão</th>
                            <td>R$ 418</td>
                            <td>R$ 500.000,00</td>
                            <td>R$ 3.300.000,00</td>
                            <td>R$ 505.393,70</td>
                        </tr>
                        <tr>
                            <th scope="row">Santa Efigênia</th>
                            <td>R$ 56.585,16</td>
                            <td>R$ 45.000,00</td>
                            <td>R$ 95.000,00</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Telêmaco Borba</th>
                            <td>-</td>
                            <td>R$ 30.000,00</td>
                            <td>R$ 36.045,04</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Vista Alegre</th>
                            <td>R$ 4.564.810,12</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <th>R$ 10.378.825,10</th>
                            <th>R$ 3.590.000,00</th>
                            <th>R$ 10.193.608,23</th>
                            <th>R$ 5.399.639,46*</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Até julho 2020</p>
        <p>Foram construídos novos prédios nas unidades de Vista Alegre e Ponta Grossa, além do setor administrativo no Portão, a conclusão da obra em Araucária e a revitalização da fachada do Colégio do Boqueirão.</p>
        <p>No âmbito pedagógico, investimos na formação continuada dos educadores, assim como em graduação para funcionários e em pós-graduação para professores, equipes administrativas e pedagógicas das unidades.</p>
        <p><strong>Educar para salvar: o envolvimento na missão</strong></p>
        <p>A espiritualidade e a solidariedade de alunos, pais e funcionários foi estimulada por meio de classes bíblicas, pelo Plano Mestre de Desenvolvimento Espiritual (PMDE), Plano Mestre Desenvolvimento da Saúde (PMDS), por projetos como o Bíblia em Minha Casa e a participação ativa nos principais programas da igreja: Impacto Esperança, Quebrando o Silêncio e Mutirão de Natal.</p>
        <p>Os números mais importantes deste quadriênio são os de batismos. Mais de 500 pessoas aceitaram a Cristo com a ajuda da Educação Adventista.</p>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="p-20">
                <table class="table table table-hover m-0">
                    <thead>
                        <tr>
                            <th>Escolas</th>
                            <th>2017</th>
                            <th>2018</th>
                            <th>2019</th>
                            <th>2020</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Araucária</th>
                            <td>32</td>
                            <td>13</td>
                            <td>24</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Boa Vista</th>
                            <td>18</td>
                            <td>19</td>
                            <td>17</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Boqueirão</th>
                            <td>40</td>
                            <td>35</td>
                            <td>14</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Castro</th>
                            <td>4</td>
                            <td>6</td>
                            <td>6</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Guarapuava</th>
                            <td>12</td>
                            <td>2</td>
                            <td>41</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Ponta Grossa</th>
                            <td>17</td>
                            <td>19</td>
                            <td>19</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <th scope="row">Portão</th>
                            <td>21</td>
                            <td>19</td>
                            <td>31</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <th scope="row">Santa Efigênia</th>
                            <td>14</td>
                            <td>11</td>
                            <td>5</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Telêmaco Borba</th>
                            <td>16</td>
                            <td>17</td>
                            <td>15</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th scope="row">Vista Alegre</th>
                            <td>5</td>
                            <td>8</td>
                            <td>4</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <th>179</th>
                            <th>149</th>
                            <th>176</th>
                            <th>5*</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p>*Até julho 2020</p>
        <p><strong>Liberdade Religiosa</strong></p>
        <p>Nesse departamento, colecionamos leis, jurisprudências, pareceres e até decisões buscando estar atualizados para melhor assessorar e orientar a comunidade adventista na solução de suas inquietações e conflitos nas áreas profissionais, acadêmicas e espirituais. Foram realizados fóruns de Liberdade Religiosa, visando atender melhor a igreja em relação aos seus direitos e deveres. Também acompanhamos as atividades do&nbsp;Fórum Regional de Liberdade Religiosa (FORLIR) e&nbsp;visitamos autoridades públicas municipais, estaduais e federais.</p>
        <div class="row">
            <div class="col-sm-3">
                <img src="r4/1.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/2.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/3.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/4.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <img src="r4/5.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/6.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/7.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/8.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <img src="r4/9.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/10.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/11.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
            <div class="col-sm-3">
                <img src="r4/12.jpg" alt="image" class="img-responsive img-rounded"/>
                <p class="m-t-15 m-b-0"></p>
            </div>
        </div>
    </body>
</html>