<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    </head>

    <body>
        <p><strong>Gilmar Dos Anjos</strong></p>
        <p><strong>Formação</strong></p>
        <ul>
        <li>Graduação em Teologia – Universidade Adventista Del Plata - UAP-Argentina (2015).</li>
        </ul>
        <p><strong>Carreira</strong></p>
        <ul>
        <li>Diretor associado de Departamento – Associação Sul Paranaense e Central Paranaense (2016 a março de 2017).</li>
        <li>Diretor de Departamento – Associação Central Paranaense - ACP (Abril de 2017 a 2020).</li>
        </ul>
        <p>Com o grande desapontamento em 1844, o Ministério de Publicações&nbsp;foi o método que Deus criou para alcançar todo o mundo com a mensagem do evangelho. “Numa reunião efetuada em Dorchester, Massachusetts, em novembro de 1848, foi-me concedida uma visão da proclamação da mensagem do assinalamento, e do dever que incumbia aos irmãos de publicarem a luz que resplandecia em nosso caminho.&nbsp;Depois da visão eu disse a meu esposo: ‘Tenho uma mensagem para ti. Deves começar a publicar um pequeno jornal e mandá-lo ao povo. Seja pequeno a princípio; mas, lendo-o o povo, mandar-te-ão meios com que imprimi-lo, e alcançará bom êxito desde o princípio’. Desde este pequeno começo foi-me mostrado assemelhar-se a torrentes de luz que circundavam o mundo.”&nbsp;(O Colportor Evangelista,&nbsp;p.&nbsp;1.1).</p>
        <p>Aí estava o método que o Senhor havia escolhido para cumprir Apocalipse 10:11 “é necessário que ainda profetizes...”</p>
        <p>Embora a tarefa de imprimir um periódico fosse enorme para as possibilidades dos pioneiros, sem dúvida foi o método mais acessível, mais rápido e de maior alcance naquele momento.</p>
        <p>Até os dias de hoje o Ministério de Publicações&nbsp;cumpre o seu dever,&nbsp;mesmo em meio aos maiores desafios desse mundo.&nbsp;Através de homens e mulheres, valorosos missionários, se levanta a bandeira da missão e segue rumo ao alvo de casa em casa, de porta em porta, até a última porta. “Nossos colportores&nbsp;devem ser evangelistas&nbsp;de&nbsp;Deus e ir&nbsp;de&nbsp;casa&nbsp;em&nbsp;casa, em lugares fora&nbsp;de&nbsp;mão, abrindo as Escrituras aos que encontrarem. Acharão os que estão ansiosos&nbsp;de&nbsp;aprender das Escrituras.”&nbsp;(O Colportor&nbsp;Evangelista, p. 39).</p>
        <p><strong>Missão:&nbsp;</strong>Transmitir a mensagem de salvação a todos os lares, até a última casa, através das publicações que exaltam a Deus e atraiam a atenção para as verdades vivas da Bíblia.</p>
        <p><strong>Visão:&nbsp;</strong>Transmitir princípios bíblicos, fazendo o evangelho ser pregado em lugares onde a nossa igreja não consegue chegar, através de colportores capacitados a desempenhar um ministério de grande êxito.</p>
        <p><strong>O</strong><strong>s</strong><strong> projetos</strong><strong> e programas</strong><strong> realizados pelo Ministério de Publicações ao longo do quadriênio</strong></p>
        <p>Em 2020, o projeto <strong>Impacto Esperança</strong> chega à sua 15ª edição e muitos são os testemunhos de pessoas alcançadas através do livro missionário.&nbsp;Isso graças ao esforço e comprometimento de nossos membros em todo o território da Associação Central Paranaense.</p>
        <ul>
        <li>2017 - 380 mil Livros missionários distribuídos</li>
        <li>2018 - 400 mil Livros missionários distribuídos</li>
        <li>2019 - 300 mil Livros missionários distribuídos</li>
        <li>2020 - 300 mil (alvo a ser alcançado no dia 31 de outubro)</li>
        </ul>
        <p>Outro programa que se tornou tradicional entre os adventistas é a&nbsp;<strong>Casa Aberta</strong>, que chama atenção pelos excelentes descontos oferecidos aos participantes. As literaturas denominacionais enriquecem a espiritualidade e o conhecimento&nbsp;dos membros. No ano de 2017, chegamos à incrível marca de R$ 348.602,97&nbsp;em literaturas, CDs, DVDs e outros produtos, sendo um destaque a nível nacional.</p>
        <p>O departamento também promoveu o Congresso Espírito de Profecia, cuja literatura é fundamental para uma ampla visão do cumprimento das profecias e dos eventos finais deste mundo. O Espírito de Profecia serve como um guia à verdade contida nas Sagradas Escrituras, que é a base para a nossa norma de fé.</p>
        <p>“Prostrei-me ante os seus pés para adorá-lo. Ele, porém, me disse: Vê, não faças isso; sou conservo teu e dos teus irmãos que mantêm o testemunho de Jesus; adora a Deus. Pois o testemunho de Jesus é o espírito da profecia.” (Apocalipse 19:10).</p>
        <p>Com o objetivo principal de levar a geração de adolescentes a ter mais comunhão com Deus através da leitura dos livros inspirados de Ellen White, mais precisamente a série “Conflito”, foi promovido o “Bom do Espírito de Profecia”, o <strong>BEP </strong><strong>Teen</strong>. Ao longo do ano, os adolescentes leram, estudaram e participaram de processos seletivos sobre o conteúdo. Para chegarem à fase final, passaram por três fases eliminatórias. Aqueles que acertaram o maior número de respostas sobre a obra estudada, foram classificados para concorrer ao prêmio final em um evento de toda a União. Através desse projeto, vimos o tremendo despertar de interesse na leitura e no conhecimento das profecias bíblicas.</p>
        <p><strong>Re</strong><strong>crutamento </strong><strong>e formação de </strong><strong>colportores</strong></p>
        <p>O Ministério de Publicações tem feito parcerias com os demais departamentos do campo local, com o objetivo de unir esforços para alcançar novos colportores, fortalecendo o programa de recrutamento. E nesse quadriênio o Ministério Jovem se tornou um dos principais parceiros devido ao interesse mútuo de envolver cada vez mais as novas gerações na Missão. No ano de 2019, o Departamento de Publicações apoiou financeiramente dois jovens escolhidos para a Missão na Amazônia.</p>
        <p>Essa parceria resultou em mais de 160 jovens interessados em participar no programa da&nbsp;<strong>Colportagem</strong><strong> Estudantil</strong>. Todas as férias, uma parte de nossos jovens sai para evangelizar colportando e ao mesmo tempo com o objetivo de conseguir os recursos para voltar para a universidade. Ao colportarem durante as férias de inverno e verão, acabam colocando em prática aquilo que aprendem na teoria durante o semestre, tornando assim ainda mais compreensível a utilidade daquilo que foi aprendido em classe. A Colportagem&nbsp;Estudantil tem sido uma faculdade à parte, com ensinamentos que serão indispensáveis na vida futura de cada jovem. “O senhor instituiu um plano pelo qual muitos dos estudantes de nossas escolas podem aprender lições práticas indispensáveis ao sucesso futuro.” (O Colportor&nbsp;Evangelista, p. 30).</p>
        <ul>
        <li>2017 - 113 Estudantes e 14 bolsas alcançadas</li>
        <li>2018 - 109 Estudantes e 6 bolsas alcançadas</li>
        <li>2019 - 125 Estudantes e 16 bolsas alcançadas</li>
        <li>2020 - 66 Estudantes e 1 bolsa alcançada (até julho de 2020)</li>
        </ul>
        <p>Total em literaturas vendidas:</p>
        <ul>
        <li>2017 - 1.636.338</li>
        <li>2018 - 1.685.054</li>
        <li>2019 - 1.411.226</li>
        <li>2020 - 378.162 (até junho de 2020)</li>
        </ul>
        <p>No ano de 2020, o projeto <strong>Sonhando Alto</strong> completou 20 anos de história e muitas foram as bênçãos recebidas. Ao longo desse período, o projeto levou milhares de jovens para as universidades, realizando assim seus sonhos de estudarem e entrarem no mercado de trabalho bem como ingressarem na obra como obreiros pastores.</p>
        <p>O projeto acontece em dois períodos do ano&nbsp;– março a maio, e setembro a novembro. Foram mais de 140 participações durante o quadriênio.</p>
        <ul>
        <li>2017 - 52</li>
        <li>2018 - 37</li>
        <li>2019 - 52</li>
        <li>2020 - Cancelamos o projeto devido à pandemia.</li>
        </ul>
        <p>Por meio do <strong>Núcleo de Formação de </strong><strong>Colportores</strong>, os candidatos previamente recomendados pelo pastor de seu distrito, são treinados e discipulados em um período de três a seis meses. Durante a capacitação, os participantes têm aulas práticas e teóricas e são avaliados. Ao final do período, quando estão aptos, os colportores são direcionados e começam a atuar em um distrito missionário.</p>
        <p>Já no <strong>Núcleo Especializado de </strong><strong>Colportores</strong>,&nbsp;os colportores permanentes são motivados para estarem cada vez mais firmes no propósito para o qual foram chamados. Nesse quadriênio mantivemos o foco no fortalecimento da vida espiritual e no relacionamento familiar de cada um. “A obra da colportagem é o meio de Deus para alcançar muitos que, de outro modo, na seriam comovidos pela verdade.” (O Colportor&nbsp;Evangelista, p. 61).</p>
        <p>Colportores&nbsp;de livros</p>
        <ul>
        <li>2017 - 5 colportores&nbsp;</li>
        <li>2018 - 27 colportores&nbsp;</li>
        <li>2019 - 20 colportores&nbsp;</li>
        <li>2020 - 14&nbsp;colportores (até junho de 2020)</li>
        </ul>
        <p>Colportores&nbsp;de revistas</p>
        <ul>
        <li>2017 - 12 colportores</li>
        <li>2018 - 3&nbsp;colportores</li>
        <li>2019 - 5&nbsp;colportores</li>
        <li>2020 - 7&nbsp;colportores&nbsp;(até junho de 2020)</li>
        </ul>
        <p>Todos os colportores da Associação Central Paranaense são extremamente envolvidos com a igreja local e apaixonados pela missão de salvar almas. Devido à grande proporção de literaturas que são distribuídas, não conseguimos mensurar o alcance evangelístico de todas as literaturas que são vendidas ou doadas, porém, sabemos que são muitos os relatos de famílias que receberam a luz da verdade por meio de um colportor. Ainda assim, conseguimos calcular nesse quadriênio aproximadamente <strong>55 batismos</strong> de forma direta pelo trabalho e discipulado de cada missionário da página impressa.</p>
        <p><strong>Agradecimentos</strong></p>
        <p>Agradecemos a Deus, em primeiro lugar, pelas bênçãos recebidas nesse quadriênio, apesar de todos os desafios e obstáculos encontrados ao longo do caminho, e pela sabedoria e orientação divina de Sua Palavra.</p>
        <p>Agradecemos também aos administradores, pelo acompanhamento e por todo apoio,&nbsp;fazendo assim tudo isso ser possível. Não podemos deixar de mencionar aqueles que passaram por esse período, os assistentes Ricardo Borba, Gabriel Bilovus, Alex Cardoso, Sandro Lobo, o Pr. Elizeu Ortiz (associado em 2018 até julho de 2019) e ao atual assistente, Paulo Orli, e que contribuíram com os resultados alcançados.</p>
        <p>Aos colegas de departamento, pela parceria formada, e aos pastores de nosso campo, pelo apoio sem medidas em seus distritos, recebendo e acolhendo nossos estudantes e colportores permanentes, bem como indicando novos recrutas para ingressarem na obra da colportagem.</p>
        <p>E, finalmente a todos os homens, mulheres e estudantes que foram os principais responsáveis por espalharem, como folhas de outono, a mensagem do advento em muitos lares através da Página Impressa. Pelo incansável esforço, em dias de sol ou chuva, de casa em casa, de porta em porta, de comércio em comércio, de empresa em empresa, seguiram firmes, falando que Cristo vai voltar.</p>
    </body>
</html>